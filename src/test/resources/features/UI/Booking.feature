@DemoUI 
Feature: Demo feature file for UI Automation

@TC_1 
Scenario: Verify the Holiday search functionality 
	Given Open the "HolidayURL" 
	Then Click on the search button 
		| ElementType     | pageName  |
		| id_searchButton | ListingPO |
		
		
@TC_2 
Scenario: Verify the live chat functionality 
	Given Open the "HolidayURL" 
	Then Click on the search button 
		| ElementType     | pageName  |
		| id_searchButton | ListingPO |
	When Click on the live chat icon 
		| ElementType     | pageName  |
		| css_chatIcon    | ListingPO |