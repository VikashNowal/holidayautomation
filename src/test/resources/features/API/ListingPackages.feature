@ListingPackages
Feature: ListingPackage API

  @ListingPackage_TC1 @Smoke
  Scenario Outline: Checking Status Code for Listing/Packages API
    Given "Guest User" sets the following headers attributes for the endpoint "ListingPackage"
      | AttributeType | AttributeValue |
      | Content-Type  | $Content-Type  |
      | authorization | $Authorization |
      | lob           | $Lob           |
      | Accept        | $Accept        |
    When "Guest User" makes a POST call for base url "HolidayService" and RequestBody "ListingPackageRequest" for endPoint "ListingPackageEndPoint"
      | AttributeType   | AttributeValue    |
      | CountryCode     | <CountryCode>     |
      | travelDate      | <travelDate>      |
      | packageId       | <packageId>       |
      | website         | <website>         |
      | DepartureCity   | <DepartureCity>   |
      | Destination     | <Destination>     |
      | affiliate       | <affiliate>       |
      | sortersCriteria | <sortersCriteria> |
    Then verify "Guest User" recieves status code as
      | CodeType |
      | 200      |
    Examples:
      | CountryCode | travelDate | packageId | website | DepartureCity | Destination | affiliate | sortersCriteria |
      | IN          | 2021-07-11 | 23726     | B2CNLR  | New Delhi     | Thailand    | MMT       | 6_1             |
      | IN          | 2021-07-11 | 23726     | android | New Delhi     | Bangkok     | MMT       | 6_0             |
      | IN          | 2021-07-11 | 23726     | B2CNLR  | New Delhi     | Thailand    | MMT       | 4_1             |
      | IN          | 2021-07-11 | 23726     | android | New Delhi     | Bangkok     | MMT       | 4_0             |
     # sorterCriteria - > id[6]->value 1 - duration High to low ,id[6]->value 0 - duration low  to lhigh
     # sorterCriteria - > id[4]-> value 1 -> duration High to low and vice versa

  @ListingPackage_TC2
  Scenario Outline: Comparing  Listing/Packages API Response for QA versus Prod
    Given "Guest User" sets the following headers attributes for the endpoint "ListingPackage"
      | AttributeType | AttributeValue |
      | Content-Type  | $Content-Type  |
      | authorization | $Authorization |
      | lob           | $Lob           |
      | Accept        | $Accept        |
    When "Guest User" makes a POST call for base url "HolidayServiceProd" and RequestBody "ListingPackage" for endPoint "ListingPackageEndPoint" on "PRODUCTION" environment
      | AttributeType | AttributeValue  |
      | CountryCode   | <CountryCode>   |
      | travelDate    | <travelDate>    |
      | packageId     | <packageId>     |
      | website       | <website>       |
      | DepartureCity | <DepartureCity> |
      | Destination   | <Destination>   |
      | affiliate     | <affiliate>     |
    When "Guest User" makes a POST call for base url "HolidayServiceQA" and RequestBody "ListingPackageRequest" for endPoint "ListingPackageEndPoint" on "STAGING_QA" environment
      | AttributeType   | AttributeValue    |
      | CountryCode     | <CountryCode>     |
      | travelDate      | <travelDate>      |
      | packageId       | <packageId>       |
      | website         | <website>         |
      | DepartureCity   | <DepartureCity>   |
      | Destination     | <Destination>     |
      | affiliate       | <affiliate>       |
      | sortersCriteria | <sortersCriteria> |
    And Compare if response on prod versus Testing Environment are same for API "ListingPackageRequest"
    Examples:
      | CountryCode | travelDate | packageId | website | DepartureCity | Destination | affiliate | sortersCriteria |
      | IN          | 2021-07-11 | 23726     | B2CNLR  | New Delhi     | Thailand    | MMT       | 6_1             |
      | IN          | 2021-07-11 | 23726     | android | New Delhi     | Bangkok     | MMT       | 4_1             |