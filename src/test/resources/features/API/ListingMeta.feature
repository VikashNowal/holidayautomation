@ListingMeta
Feature: ListingMeta API

  @ListingMetaAPI_TC1 @Smoke
  Scenario: Verify the ListingMeta api is giving 200
    Given "Guest User" sets the following headers attributes for the endpoint "ListingMetaEndpoint"
      | AttributeType | AttributeValue |
      | Content-Type  | $Content-Type  |
      | authorization | $Authorization |
      | lob           | $Lob           |
      | Accept        | $Accept        |
    When "Guest User" sets the following headers attributes for the endpoint "ListingMetaEndpoint"
      | AttributeType | AttributeValue |
      | Content-Type  | $Content-Type  |
    When "Guest User" makes a POST call for base url "HolidayService" using "no parameters" with the body "ListingMeta" for "ListingMetaEndpoint"
      | AttributeName | AttributeValue | AttributeUsage |
    Then verify "Guest User" recieves status code as
      | CodeType |
      | 200      |
    And "Guest User" verifies "below attributes" from the above response
      | Key     | Values |
      | success | true   |


  @ListingMetaAPI_TC2
  Scenario: Verify the ListingMeta Respons on QA Versus production is Equal
    Given "Guest User" sets the following headers attributes for the endpoint "ListingMetaEndpoint"
      | AttributeType | AttributeValue |
      | Content-Type  | $Content-Type  |
      | authorization | $Authorization |
      | lob           | $Lob           |
      | Accept        | $Accept        |
    When "Guest User" sets the following headers attributes for the endpoint "ListingMetaEndpoint"
      | AttributeType | AttributeValue |
      | Content-Type  | $Content-Type  |
    When "Guest User" makes a POST call for base url "HolidayServiceProd" using "no parameters" with the body "ListingMeta" for endpoint "ListingMetaEndpoint" on "PRODUCTION" environment
      | AttributeName | AttributeValue | AttributeUsage |
    Then verify "Guest User" recieves status code as
      | CodeType |
      | 200      |
    When "Guest User" makes a POST call for base url "HolidayServiceQA" using "no parameters" with the body "ListingMeta" for endpoint "ListingMetaEndpoint" on "STAGING_QA" environment
      | AttributeName | AttributeValue | AttributeUsage |
    Then verify "Guest User" recieves status code as
      | CodeType |
      | 200      |
    And Compare if response on prod versus Testing Environment are same for API "ListingMeta"


