@ActivityChange
Feature: ActivityChange API

  @ActivityChange_TC1 @Smoke
  Scenario Outline: Checking Status Code for activity/change API
    Given "Guest User" sets the following headers attributes for the endpoint "DynamicDetails"
      | AttributeType | AttributeValue |
      | Content-Type  | $Content-Type  |
      | authorization | $Authorization |
      | lob           | $Lob           |
      | Accept        | $Accept        |
    When "Guest User" makes a POST call for base url "HolidayService" and RequestBody "DynamicDetails" for endPoint "DynamicDetails"
      | AttributeType | AttributeValue  |
      | CountryCode   | <CountryCode>   |
      | travelDate    | <travelDate>    |
      | packageId     | <packageId>     |
      | website       | <website>       |
      | DepartureCity | <DepartureCity> |
      | Adult         | <Adult>         |
      | staySequence  | <staySequence>  |
      | lob           | <lob>           |
      | funnel        | <funnel>        |
      | affiliate     | <affiliate>     |
    Then verify "Guest User" recieves status code as
      | CodeType |
      | 200      |
    And Store the "dynamicId" from the response
    When "Guest User" sets the following headers attributes for the endpoint "ActivityChangeListingEndPoint"
      | AttributeType | AttributeValue |
      | Content-Type  | $Content-Type  |
      | authorization | $Authorization |
      | lob           | $Lob           |
      | Accept        | $Accept        |
    When "Guest User" makes a POST call for base url "HolidayService" and RequestBody "ActivityChangeListing" for endPoint "ActivityChangeListingEndPoint"
      | AttributeType | AttributeValue  |
      | CountryCode   | <CountryCode>   |
      | travelDate    | <travelDate>    |
      | packageId     | <packageId>     |
      | website       | <website>       |
      | DepartureCity | <DepartureCity> |
      | Adult         | <Adult>         |
      | staySequence  | <staySequence>  |
      | lob           | <lob>           |
      | funnel        | <funnel>        |
      | affiliate     | <affiliate>     |
    Then verify "Guest User" recieves status code as
      | CodeType |
      | 200      |
    Examples:
      | CountryCode | travelDate | packageId | website | DepartureCity | Adult | staySequence | lob      | funnel | affiliate |
      | IN          | 2021-07-11 | 23726     | B2CNLR  | New Delhi     | 2     | 1            | Holidays | HLD    | MMT       |
      | IN          | 2021-07-11 | 23726     | android | New Delhi     | 2     | 1            | Holidays | HLD    | MMT       |


  @ActivityChange_TC2
  Scenario Outline: Checking Nodes present in Activity/Change API
    Given "Guest User" sets the following headers attributes for the endpoint "DynamicDetails"
      | AttributeType | AttributeValue |
      | Content-Type  | $Content-Type  |
      | authorization | $Authorization |
      | lob           | $Lob           |
      | Accept        | $Accept        |
    When "Guest User" makes a POST call for base url "HolidayService" and RequestBody "DynamicDetails" for endPoint "DynamicDetails"
      | AttributeType | AttributeValue  |
      | CountryCode   | <CountryCode>   |
      | travelDate    | <travelDate>    |
      | packageId     | <packageId>     |
      | website       | <website>       |
      | DepartureCity | <DepartureCity> |
      | Adult         | <Adult>         |
      | staySequence  | <staySequence>  |
      | lob           | <lob>           |
      | funnel        | <funnel>        |
      | affiliate     | <affiliate>     |
    Then verify "Guest User" recieves status code as
      | CodeType |
      | 200      |
    And Store the "dynamicId" from the response
    And Store the Response of DynamicDetails with key "dynamicDetailsResponse"
    When "Guest User" sets the following headers attributes for the endpoint "ActivityChangeListingEndPoint"
      | AttributeType | AttributeValue |
      | Content-Type  | $Content-Type  |
      | authorization | $Authorization |
      | lob           | $Lob           |
      | Accept        | $Accept        |
    When "Guest User" makes a POST call for base url "HolidayService" and RequestBody "ActivityChangeListing" for endPoint "ActivityChangeListingEndPoint"
      | AttributeType | AttributeValue  |
      | CountryCode   | <CountryCode>   |
      | travelDate    | <travelDate>    |
      | packageId     | <packageId>     |
      | website       | <website>       |
      | DepartureCity | <DepartureCity> |
      | Adult         | <Adult>         |
      | staySequence  | <staySequence>  |
      | lob           | <lob>           |
      | funnel        | <funnel>        |
      | affiliate     | <affiliate>     |
    Then verify "Guest User" recieves status code as
      | CodeType |
      | 200      |
    And verify package PriceMap is coming in response
    And verify activities cityDetails are as per Activity DaywiseCityMap
      | AttributeType | AttributeValue |
      | staySequence  | <staySequence> |
    Examples:
      | CountryCode | travelDate | packageId | website | DepartureCity | Adult | staySequence | lob      | funnel | affiliate |
      | IN          | 2021-09-01 | 51222     | B2CNLR  | New Delhi     | 2     | 1            | Holidays | HLD    | MMT       |
      | IN          | 2021-09-01 | 27682     | android | New Delhi     | 2     | 1            | Holidays | HLD    | MMT       |