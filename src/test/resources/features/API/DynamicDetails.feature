@DynamicDetail
Feature: DynamicDetails API

  @DynamicDetail_TC1 @Smoke
  Scenario Outline: Checking Status Code for dynamicDetails API
    Given "Guest User" sets the following headers attributes for the endpoint "DynamicDetails"
      | AttributeType | AttributeValue |
      | Content-Type  | $Content-Type  |
      | authorization | $Authorization |
      | lob           | $Lob           |
      | Accept        | $Accept        |
    When "Guest User" makes a POST call for base url "HolidayService" and RequestBody "DynamicDetails" for endPoint "DynamicDetails"
      | AttributeType | AttributeValue  |
      | CountryCode   | <CountryCode>   |
      | travelDate    | <travelDate>    |
      | packageId     | <packageId>     |
      | website       | <website>       |
      | DepartureCity | <DepartureCity> |
      | RoomCount     | <Adult>         |
    Then verify "Guest User" recieves status code as
      | CodeType |
      | 200      |

    Examples:
      | CountryCode | travelDate | packageId | website | DepartureCity | Adult |
      | IN          | 2021-07-11 | 23726     | B2CNLR  | New Delhi     | 2     |
      | IN          | 2021-07-11 | 23726     | android | New Delhi     | 2     |

  @DynamicDetail_TC2
  Scenario Outline: Checking Inclusion details of a Package for dynamicDetails API
    Given "Guest User" sets the following headers attributes for the endpoint "DynamicDetails"
      | AttributeType | AttributeValue |
      | Content-Type  | $Content-Type  |
      | authorization | $Authorization |
      | lob           | $Lob           |
      | Accept        | $Accept        |
    When "Guest User" makes a POST call for base url "HolidayService" and RequestBody "DynamicDetails" for endPoint "DynamicDetails"
      | AttributeType | AttributeValue  |
      | CountryCode   | <CountryCode>   |
      | travelDate    | <travelDate>    |
      | packageId     | <packageId>     |
      | website       | <website>       |
      | DepartureCity | <DepartureCity> |
      | RoomCount     | <Adult>         |
    Then verify "Guest User" recieves status code as
      | CodeType |
      | 200      |
    And verify inclusions Components are present in DetailResponse

    Examples:
      | CountryCode | travelDate | packageId | website | DepartureCity | Adult |
      | IN          | 2021-07-11 | 23726     | B2CNLR  | New Delhi     | 2     |
      | IN          | 2021-07-11 | 23726     | android | New Delhi     | 2     |

  @DynamicDetail_TC3
  Scenario Outline: Checking coupon Nodes & Prices node for dynamicDetails API
    Given "Guest User" sets the following headers attributes for the endpoint "DynamicDetails"
      | AttributeType | AttributeValue |
      | Content-Type  | $Content-Type  |
      | authorization | $Authorization |
      | lob           | $Lob           |
      | Accept        | $Accept        |
    When "Guest User" makes a POST call for base url "HolidayService" and RequestBody "DynamicDetails" for endPoint "DynamicDetails"
      | AttributeType | AttributeValue  |
      | CountryCode   | <CountryCode>   |
      | travelDate    | <travelDate>    |
      | packageId     | <packageId>     |
      | website       | <website>       |
      | DepartureCity | <DepartureCity> |
      | RoomCount     | <Adult>         |
    Then verify "Guest User" recieves status code as
      | CodeType |
      | 200      |
    And verify couponCode and coupon details section is Present
    And validate prices are correct in details Response

    Examples:
      | CountryCode | travelDate | packageId | website | DepartureCity | Adult |
      | IN          | 2021-07-11 | 23726     | B2CNLR  | New Delhi     | 2     |
      | IN          | 2021-07-11 | 23726     | android | New Delhi     | 2     |

  @DynamicDetail_TC4
  Scenario Outline: Checking Hotel Component Details in Dynamic Details API
    Given "Guest User" sets the following headers attributes for the endpoint "DynamicDetails"
      | AttributeType | AttributeValue |
      | Content-Type  | $Content-Type  |
      | authorization | $Authorization |
      | lob           | $Lob           |
      | Accept        | $Accept        |
    When "Guest User" makes a POST call for base url "HolidayService" and RequestBody "DynamicDetails" for endPoint "DynamicDetails"
      | AttributeType | AttributeValue  |
      | CountryCode   | <CountryCode>   |
      | travelDate    | <travelDate>    |
      | packageId     | <packageId>     |
      | website       | <website>       |
      | DepartureCity | <DepartureCity> |
      | RoomCount     | <Adult>         |
    Then verify "Guest User" recieves status code as
      | CodeType |
      | 200      |
    And verify hotel components in API response

    Examples:
      | CountryCode | travelDate | packageId | website | DepartureCity | Adult |
      | IN          | 2021-07-11 | 23726     | B2CNLR  | Amritsar      | 2     |
      | IN          | 2021-07-11 | 23726     | android | New Delhi     | 2     |
      | IN          | 2021-07-11 | 41909     | B2CNLR  | Chandigarh    | 2     |
      | IN          | 2021-07-11 | 50906     | B2CNLR  | Kolkata       | 2     |

  @DynamicDetail_TC5
  Scenario Outline: Checking Flights Component Details in Dynamic Details API
    Given "Guest User" sets the following headers attributes for the endpoint "DynamicDetails"
      | AttributeType | AttributeValue |
      | Content-Type  | $Content-Type  |
      | authorization | $Authorization |
      | lob           | $Lob           |
      | Accept        | $Accept        |
    When "Guest User" makes a POST call for base url "HolidayService" and RequestBody "DynamicDetails" for endPoint "DynamicDetails"
      | AttributeType | AttributeValue  |
      | CountryCode   | <CountryCode>   |
      | travelDate    | <travelDate>    |
      | packageId     | <packageId>     |
      | website       | <website>       |
      | DepartureCity | <DepartureCity> |
      | RoomCount     | <Adult>         |
    Then verify "Guest User" recieves status code as
      | CodeType |
      | 200      |
    And verify flight components in API response

    Examples:
      | CountryCode | travelDate | packageId | website | DepartureCity | Adult |
      | IN          | 2021-07-11 | 23726     | B2CNLR  | Amritsar      | 2     |
      | IN          | 2021-07-11 | 23726     | android | New Delhi     | 2     |
      | IN          | 2021-07-11 | 41909     | B2CNLR  | Chandigarh    | 2     |
      | IN          | 2021-07-11 | 50906     | B2CNLR  | Kolkata       | 2     |

  @DynamicDetail_TC6
  Scenario Outline: Checking Transfers Component Details in Dynamic Details API
    Given "Guest User" sets the following headers attributes for the endpoint "DynamicDetails"
      | AttributeType | AttributeValue  |
      | Content-Type  | $Content-Type   |
      | authorization | $Authorization  |
      | lob           | $Lob            |
      | Accept        | $Accept         |
      | DepartureCity | <DepartureCity> |
      | RoomCount     | <Adult>         |
    When "Guest User" makes a POST call for base url "HolidayService" and RequestBody "DynamicDetails" for endPoint "DynamicDetails"
      | AttributeType | AttributeValue |
      | CountryCode   | <CountryCode>  |
      | travelDate    | <travelDate>   |
      | packageId     | <packageId>    |
      | website       | <website>      |
    Then verify "Guest User" recieves status code as
      | CodeType |
      | 200      |
    And verify Transfers components in API response if included

    Examples:
      | CountryCode | travelDate | packageId | website | DepartureCity | Adult |
      | IN          | 2021-07-11 | 41909     | B2CNLR  | Chandigarh    | 2     |
      | IN          | 2021-07-11 | 50906     | B2CNLR  | Kolkata       | 2     |

  @DynamicDetail_TC7
  Scenario Outline: Checking Visa Component Details in Dynamic Details API
    Given "Guest User" sets the following headers attributes for the endpoint "DynamicDetails"
      | AttributeType | AttributeValue  |
      | Content-Type  | $Content-Type   |
      | authorization | $Authorization  |
      | lob           | $Lob            |
      | Accept        | $Accept         |
      | DepartureCity | <DepartureCity> |
      | RoomCount     | <Adult>         |
    When "Guest User" makes a POST call for base url "HolidayService" and RequestBody "DynamicDetails" for endPoint "DynamicDetails"
      | AttributeType | AttributeValue  |
      | CountryCode   | <CountryCode>   |
      | travelDate    | <travelDate>    |
      | packageId     | <packageId>     |
      | website       | <website>       |
      | DepartureCity | <DepartureCity> |
      | RoomCount     | <Adult>         |
    Then verify "Guest User" recieves status code as
      | CodeType |
      | 200      |
    And verify Visa component details in API response if included

    Examples:
      | CountryCode | travelDate | packageId | website | DepartureCity | Adult |
      | IN          | 2021-07-11 | 41909     | B2CNLR  | New Delhi     | 2     |

  @DynamicDetail_TC8
  Scenario Outline: Checking Activity Component Details in Dynamic Details API
    Given "Guest User" sets the following headers attributes for the endpoint "DynamicDetails"
      | AttributeType | AttributeValue |
      | Content-Type  | $Content-Type  |
      | authorization | $Authorization |
      | lob           | $Lob           |
      | Accept        | $Accept        |
    When "Guest User" makes a POST call for base url "HolidayService" and RequestBody "DynamicDetails" for endPoint "DynamicDetails"
      | AttributeType | AttributeValue  |
      | CountryCode   | <CountryCode>   |
      | travelDate    | <travelDate>    |
      | packageId     | <packageId>     |
      | website       | <website>       |
      | DepartureCity | <DepartureCity> |
      | RoomCount     | <Adult>         |
    Then verify "Guest User" recieves status code as
      | CodeType |
      | 200      |
    And verify Activity component details in API response if included
    Examples:
      | CountryCode | travelDate | packageId | website | DepartureCity | Adult |
      | IN          | 2021-07-11 | 23726     | android | New Delhi     | 2     |
      | IN          | 2021-07-11 | 50906     | B2CNLR  | New Delhi     | 2     |
