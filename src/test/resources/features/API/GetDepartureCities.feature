@DepartureCities
Feature: DepartureCities API


  @DepartureCities_TC1 @Smoke
  Scenario Outline: Checking Status Code for DepartureCities API
    Given "Guest User" sets the following headers attributes for the endpoint "DepartureCities"
      | AttributeType | AttributeValue |
      | Content-Type  | $Content-Type  |
    When "Guest User" makes a get call for base url "HolidayService" with "below attributes" for "DepartureCities"
      | AttributeName | AttributeValue | AttributeUsage |
      | Affiliate     | <affiliate>    | param          |
      | Funnel        | <funnel>       | param          |
      | Channel       | <channel>      | param          |
      | Website       | <website>      | param          |
    Then verify "Guest User" recieves status code as
      | CodeType |
      | 200      |
    Examples:
      | affiliate | funnel | channel | website |
      | MMT       | HLD    | B2CNLR  | IN      |
      | GI        | HLD    | B2CNLR  | IN      |


  @DepartureCities_TC2
  Scenario Outline: Checking Status Code for DepartureCities API
    Given "Guest User" sets the following headers attributes for the endpoint "DepartureCities"
      | AttributeType | AttributeValue |
      | Content-Type  | $Content-Type  |
    When "Guest User" makes a get call for base url "HolidayService" with "below attributes" for "DepartureCities"
      | AttributeName | AttributeValue | AttributeUsage |
      | Affiliate     | <affiliate>    | param          |
      | Funnel        | <funnel>       | param          |
      | Channel       | <channel>      | param          |
      | Website       | <website>      | param          |
    Then verify "Guest User" recieves status code as
      | CodeType |
      | 200      |
    And Verify that the DepartureCities repsonse have listOfDepartureCities
    Examples:
      | affiliate | funnel | channel | website |
      | MMT       | HLD    | B2CNLR  | IN      |
      | GI        | HLD    | B2CNLR  | IN      |


  @DepartureCities_TC3
  Scenario Outline: Checking DepartureCities API for Staging Versus QA
    Given "Guest User" sets the following headers attributes for the endpoint "DepartureCities"
      | AttributeType | AttributeValue |
      | Content-Type  | $Content-Type  |
    When "Guest User" makes a get call for base url "HolidayServiceQA" with "below attributes" for endpoint "DepartureCities" on "STAGING_QA" environment
      | AttributeName | AttributeValue | AttributeUsage |
      | Affiliate     | <affiliate>    | param          |
      | Funnel        | <funnel>       | param          |
      | Channel       | <channel>      | param          |
      | Website       | <website>      | param          |
    Then verify "Guest User" recieves status code as
      | CodeType |
      | 200      |
    When "Guest User" makes a get call for base url "HolidayServiceProd" with "below attributes" for endpoint "DepartureCities" on "PRODUCTION" environment
      | AttributeName | AttributeValue | AttributeUsage |
      | Affiliate     | <affiliate>    | param          |
      | Funnel        | <funnel>       | param          |
      | Channel       | <channel>      | param          |
      | Website       | <website>      | param          |
    Then verify "Guest User" recieves status code as
      | CodeType |
      | 200      |
    And Compare if response on prod versus Testing Environment are same for API "DepartureCities"
    Examples:
      | affiliate | funnel | channel | website |
      | MMT       | HLD    | B2CNLR  | IN      |
      | GI        | HLD    | B2CNLR  | IN      |