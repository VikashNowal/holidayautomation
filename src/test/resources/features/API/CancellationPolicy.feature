@CancellationDetails
Feature: Cancellation Details

  @CancellationDetails_TC1
  Scenario Outline: Verify the cancellation Detail api is giving 200
    Given "Guest User" sets the following headers attributes for the endpoint "DynamicDetails"
      | AttributeType | AttributeValue |
      | Content-Type  | $Content-Type  |
      | authorization | $Authorization |
      | lob           | $Lob           |
      | Accept        | $Accept        |
    When "Guest User" makes a POST call for base url "HolidayService" and RequestBody "DynamicDetailsRequest" for endPoint "DynamicDetails"
      | AttributeType | AttributeValue  |
      | CountryCode   | <CountryCode>   |
      | travelDate    | <travelDate>    |
      | packageId     | <packageId>     |
      | website       | <website>       |
      | DepartureCity | <DepartureCity> |
      | RoomCount     | <Adult>         |
    Then verify "Guest User" recieves status code as
      | CodeType |
      | 200      |
    And Store the "dynamicId" from the response
    When "Guest User" sets the following headers attributes for the endpoint "CancellationDetailRequest"
      | AttributeType | AttributeValue |
      | Content-Type  | $Content-Type  |
    When "Guest User" makes a POST call for base url "HolidayService" using "no parameters" with the body "CancellationDetailRequest" for "CancellationDetails"
      | AttributeName | AttributeValue | AttributeUsage |
    Then verify "Guest User" recieves status code as
      | CodeType |
      | 200      |
    And "Guest User" verifies "below attributes" from the above response
      | Key     | Values |
      | success | true   |

    Examples:
      | CountryCode | travelDate | packageId | website | DepartureCity | Adult |
      | IN          | 2021-07-11 | 23726     | B2CNLR  | New Delhi     | 2     |

  @CancellationDetails_TC2
  Scenario Outline: Verify the first penalty amount is less than or Equal to second penalty amount
    Given "Guest User" sets the following headers attributes for the endpoint "DynamicDetails"
      | AttributeType | AttributeValue |
      | Content-Type  | $Content-Type  |
      | authorization | $Authorization |
      | lob           | $Lob           |
      | Accept        | $Accept        |
    When "Guest User" makes a POST call for base url "HolidayService" and RequestBody "DynamicDetailsRequest" for endPoint "DynamicDetails"
      | AttributeType | AttributeValue  |
      | CountryCode   | <CountryCode>   |
      | travelDate    | <travelDate>    |
      | packageId     | <packageId>     |
      | website       | <website>       |
      | DepartureCity | <DepartureCity> |
      | RoomCount     | <Adult>         |
    Then verify "Guest User" recieves status code as
      | CodeType |
      | 200      |
    And Store the "dynamicId" from the response
    When "Guest User" sets the following headers attributes for the endpoint "CancellationDetailRequest"
      | AttributeType | AttributeValue |
      | Content-Type  | $Content-Type  |
    When "Guest User" makes a POST call for base url "HolidayService" using "no parameters" with the body "CancellationDetailRequest" for "CancellationDetails"
      | AttributeName | AttributeValue | AttributeUsage |
    Then verify "Guest User" recieves status code as
      | CodeType |
      | 200      |
    And "Guest User" compare the first penalty amount should be less than equal to second penalty amount

    Examples:
      | CountryCode | travelDate | packageId | website | DepartureCity | Adult |
      | IN          | 2021-07-11 | 23726     | B2CNLR  | New Delhi     | 2     |

  @CancellationDetails_TC3
  Scenario Outline: Verify the first penalty date should be before the second penalty date
    Given "Guest User" sets the following headers attributes for the endpoint "DynamicDetails"
      | AttributeType | AttributeValue |
      | Content-Type  | $Content-Type  |
      | authorization | $Authorization |
      | lob           | $Lob           |
      | Accept        | $Accept        |
    When "Guest User" makes a POST call for base url "HolidayService" and RequestBody "DynamicDetailsRequest" for endPoint "DynamicDetails"
      | AttributeType | AttributeValue  |
      | CountryCode   | <CountryCode>   |
      | travelDate    | <travelDate>    |
      | packageId     | <packageId>     |
      | website       | <website>       |
      | DepartureCity | <DepartureCity> |
      | RoomCount     | <Adult>         |
    Then verify "Guest User" recieves status code as
      | CodeType |
      | 200      |
    And Store the "dynamicId" from the response
    When "Guest User" sets the following headers attributes for the endpoint "CancellationDetailRequest"
      | AttributeType | AttributeValue |
      | Content-Type  | $Content-Type  |
    When "Guest User" makes a POST call for base url "HolidayService" using "no parameters" with the body "CancellationDetailRequest" for "CancellationDetails"
      | AttributeName | AttributeValue | AttributeUsage |
    Then verify "Guest User" recieves status code as
      | CodeType |
      | 200      |
    And "Guest User" compare the first penalty date should be before the second penalty date

    Examples:
      | CountryCode | travelDate | packageId | website | DepartureCity | Adult |
      | IN          | 2021-07-11 | 23726     | B2CNLR  | New Delhi     | 2     |

  @CancellationDetails_TC4
  Scenario Outline: Verify that Non-Refundable should be TRUE if penalty amount is equal to the package price
    Given "Guest User" sets the following headers attributes for the endpoint "DynamicDetails"
      | AttributeType | AttributeValue |
      | Content-Type  | $Content-Type  |
      | authorization | $Authorization |
      | lob           | $Lob           |
      | Accept        | $Accept        |
    When "Guest User" makes a POST call for base url "HolidayService" and RequestBody "DynamicDetailsRequest" for endPoint "DynamicDetails"
      | AttributeType | AttributeValue  |
      | CountryCode   | <CountryCode>   |
      | travelDate    | <travelDate>    |
      | packageId     | <packageId>     |
      | website       | <website>       |
      | DepartureCity | <DepartureCity> |
      | RoomCount     | <Adult>         |
    Then verify "Guest User" recieves status code as
      | CodeType |
      | 200      |
    And Store the "dynamicId" from the response
    And "Guest User" save the response for below attributes
      | Key             |
      | discountedPrice |
      | serviceTax      |
    When "Guest User" sets the following headers attributes for the endpoint "CancellationDetailRequest"
      | AttributeType | AttributeValue |
      | Content-Type  | $Content-Type  |
    When "Guest User" makes a POST call for base url "HolidayService" using "no parameters" with the body "CancellationDetailRequest" for "CancellationDetails"
      | AttributeName | AttributeValue | AttributeUsage |
    Then verify "Guest User" recieves status code as
      | CodeType |
      | 200      |
    And "Guest User" verify that Non Refundable should be TRUE when Penalty amount is equal to package price

    Examples:
      | CountryCode | travelDate | packageId | website | DepartureCity | Adult |
      | IN          | 2021-07-11 | 23726     | B2CNLR  | New Delhi     | 2     |

  @CancellationDetails_TC5
  Scenario Outline: Checking Cancellation Detail API for Staging Versus QA
    Given "Guest User" sets the following headers attributes for the endpoint "DynamicDetails"
      | AttributeType | AttributeValue |
      | Content-Type  | $Content-Type  |
      | authorization | $Authorization |
      | lob           | $Lob           |
      | Accept        | $Accept        |
    When "Guest User" makes a POST call for base url "HolidayService" and RequestBody "DynamicDetailsRequest" for endPoint "DynamicDetails"
      | AttributeType | AttributeValue  |
      | CountryCode   | <CountryCode>   |
      | travelDate    | <travelDate>    |
      | packageId     | <packageId>     |
      | website       | <website>       |
      | DepartureCity | <DepartureCity> |
      | RoomCount     | <Adult>         |
    Then verify "Guest User" recieves status code as
      | CodeType |
      | 200      |
    And Store the "dynamicId" from the response
    When "Guest User" sets the following headers attributes for the endpoint "CancellationDetail"
      | AttributeType | AttributeValue |
      | Content-Type  | $Content-Type  |
    When "Guest User" makes a POST call for base url "HolidayServiceQA" using "no parameters" with the body "CancellationDetailRequest" for endpoint "CancellationDetails" on "STAGING_QA" environment
      | AttributeName | AttributeValue | AttributeUsage |
    Then verify "Guest User" recieves status code as
      | CodeType |
      | 200      |
    When "Guest User" sets the following headers attributes for the endpoint "DynamicDetails"
      | AttributeType | AttributeValue |
      | Content-Type  | $Content-Type  |
      | authorization | $Authorization |
      | lob           | $Lob           |
      | Accept        | $Accept        |
    When "Guest User" makes a POST call for base url "HolidayService" and RequestBody "DynamicDetailsRequest" for endPoint "DynamicDetails"
      | AttributeType | AttributeValue  |
      | CountryCode   | <CountryCode>   |
      | travelDate    | <travelDate>    |
      | packageId     | <packageId>     |
      | website       | <website>       |
      | DepartureCity | <DepartureCity> |
      | RoomCount     | <Adult>         |
    Then verify "Guest User" recieves status code as
      | CodeType |
      | 200      |
    And Store the "dynamicId" from the response
    When "Guest User" sets the following headers attributes for the endpoint "CancellationDetail"
      | AttributeType | AttributeValue |
      | Content-Type  | $Content-Type  |
    When "Guest User" makes a POST call for base url "HolidayServiceProd" using "no parameters" with the body "CancellationDetailRequest" for endpoint "CancellationDetails" on "PRODUCTION" environment
      | AttributeName | AttributeValue | AttributeUsage |
    Then verify "Guest User" recieves status code as
      | CodeType |
      | 200      |
    And Compare if response on prod versus Testing Environment are same for API "CancellationDetail"
    And Compare the response on prod versus Testing Environment are same for API "CancellationDetail"
      | DtoClasses                |
      | CancellationPolicyRequest |
      | dateChangePolicyRequest   |

    Examples:
      | CountryCode | travelDate | packageId | website | DepartureCity | Adult |
      | IN          | 2021-07-11 | 23726     | B2CNLR  | New Delhi     | 2     |