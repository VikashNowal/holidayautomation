@Demoo
Feature: Demo feature file for API Automation

  @TestCase1
  Scenario: Demo scenario for POST call
    Given "Guest User" sets the following headers attributes for the endpoint "CreateTicket"
      | AttributeType | AttributeValue |
      | Content-Type  | $Content-Type  |
      | mmt-auth      | $MmtAuth       |
      | Cookie        | $Cookie        |
    And "Guest User" create a dynamic request payload for "CreateTicketRequest"
      | primarytype | secondarytype |
      | OBT         | Singapore     |
    Then verify "Guest User" recieves status code as
      | CodeType |
      | 200      |
    When "Guest User" makes a POST call for base url "CreatTicketBaseURL" using "no paramters" with the body "CreateTicketRequest" for "CreateTicket"
      | AttributeName | AttributeValue | AttributeUsage |
    Then verify "Guest User" recieves status code as
      | CodeType |
      | 200      |
    And "retailUser" verifies "below attributes" from the above response
      | Key   | Values  |
      | state | SUCCESS |
    And validate that the attributes below are matching in "addressDeliveryAddress" response
      | Attributes    |
      | state         |
      | errorCode     |
      | correlationid |
      | description   |
    Then "Guest User" gets "guid" of "the newly created cart"
    Then "Guest User" save the response for below attributes
      | Attributes  |
      | currentPage |
    Then "Guest User" assert the response for below attributes
      | Attributes |
      | 0          |
    And Store the "dynamicId" from the response
    Then "Retail User" verifes following attributes values are not Null from "add Bundle API " response
      | Attributes  |
      | displayName |
    And "Guest User" save the response for below attributes
      | Key             |
      | discountedPrice |
      | ServiceTax      |
    And Compare if response on prod versus Testing Environment are same for API "DepartureCities"


  @TestCase2
  Scenario Outline: Demo scenario for GET call
    Given "Guest User" sets the following headers attributes for the endpoint "VisaSearch"
      | AttributeType | AttributeValue |
      | Content-Type  | $Content-Type  |
      | authorization | $Authorization |
      | lob           | $Lob           |
    When "Guest User" makes a get call for base url "VisaSearchBaseUrl" with "below attributes" for "VisaSearch" for "PRODUCTION" environment
      | AttributeName | AttributeValue | AttributeUsage |
      | CountryCode   | <CountryCode>  | param          |
      | FromDate      | <FromDate>     | param          |
      | PaxAge        | <PaxAge>       | param          |
    Then verify "Guest User" recieves status code as
      | CodeType |
      | 200      |
    Examples:
      | CountryCode | FromDate   | PaxAge |
      | ae          | 20-04-2021 | 25     |
      | sg          | 20-04-2021 | 25     |

  Scenario Outline: Verify the cancellation Detail api is giving 200
    Given "Guest User" sets the following headers attributes for the endpoint "DynamicDetails"
      | AttributeType | AttributeValue |
      | Content-Type  | $Content-Type  |
      | authorization | $Authorization |
      | lob           | $Lob           |
      | Accept        | $Accept        |
    And "Guest User" sets body with following payLoads
      | Key         | Value         | Type    |
      | name        | <Name>        | String  |
      | isGuestUser | <IsGuestUser> | boolean |

    Examples:
      | Name   | IsGuestUser |
      | vikash | true        |