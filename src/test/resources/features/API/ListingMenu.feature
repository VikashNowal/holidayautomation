@ListingMenu
Feature: Menu API on Listing page

  @ListingMenu_TC1
  Scenario: Checking Cancellation Detail API for Staging Versus QA
    Given "Guest User" sets the following headers attributes for the endpoint "ListingMenu"
      | AttributeType | AttributeValue |
      | Content-Type  | $Content-Type  |
    When "Guest User" makes a POST call for base url "HolidayServiceQA" using "no parameters" with the body "ListingMenuRequest" for endpoint "ListingMenu" on "STAGING_QA" environment
      | AttributeName | AttributeValue | AttributeUsage |
    Then verify "Guest User" recieves status code as
      | CodeType |
      | 200      |
    When "Guest User" sets the following headers attributes for the endpoint "ListingMenu"
      | AttributeType | AttributeValue |
      | Content-Type  | $Content-Type  |
    When "Guest User" makes a POST call for base url "HolidayServiceProd" using "no parameters" with the body "ListingMenuRequest" for endpoint "ListingMenu" on "PRODUCTION" environment
      | AttributeName | AttributeValue | AttributeUsage |
    Then verify "Guest User" recieves status code as
      | CodeType |
      | 200      |
    And Compare if response on prod versus Testing Environment are same for API "ListingMenu"