package com.mmt.automation.api.test.parsers.listing;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.mmt.automation.api.test.pojo.listing.CityDetailsRequest;
import com.mmt.automation.test.common.interfaces.APIParsersBaseClass;
import io.restassured.response.Response;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.json.JSONArray;
import org.json.JSONObject;
import org.testng.Assert;

import java.io.IOException;
import java.util.List;

public class DepartureCitiesParser implements APIParsersBaseClass {

    private static final Logger logger = LogManager.getLogger(DepartureCitiesParser.class);
    private static DepartureCitiesParser departureCitiesParser = null;
    private JSONObject parser;
    private ObjectMapper mapper;

    public static DepartureCitiesParser getInstance() {
        if (departureCitiesParser == null)
            departureCitiesParser = new DepartureCitiesParser();

        return departureCitiesParser;
    }

    public void validateDepeartureCitiesNodes(Response response) throws IOException {
        parser = new JSONObject(response.asString());
        Assert.assertNotNull(parser.get("listOfDepartureCity"));
        Assert.assertEquals(parser.get("statusMessage"), "SUCCESS");
        JSONArray listOfCities = (JSONArray) parser.get("listOfDepartureCity");
        mapper = new ObjectMapper();
        CityDetailsRequest cityDetailprod;
        for (Object cityObject : listOfCities) {
            cityDetailprod = mapper.readValue(cityObject.toString(), CityDetailsRequest.class);
            if (Integer.parseInt(String.valueOf(cityDetailprod.getId()))
                    != -1) { // This check is needed as 1 entry is -1 in data and having null values so failing overall Assertions
                System.out.println("--->" + cityDetailprod.getId());
                Assert.assertNotNull(cityDetailprod.getCountryId());
                Assert.assertNotNull(cityDetailprod.getAirportCode());
                Assert.assertNotNull(cityDetailprod.getCountryName());
                Assert.assertNotNull(cityDetailprod.getId());
                Assert.assertNotNull(cityDetailprod.getName());
                Assert.assertNotNull(cityDetailprod.getTopListed());
                Assert.assertNotNull(cityDetailprod.getAdditionalProperties());
            }
        }

    }

    @Override public void compareQAversusProdResponse(Response responseQA, Response responseProd) throws IOException {

        JSONObject jsonObjectQA = new JSONObject(responseQA.asString());
        JSONObject jsonObjectProd = new JSONObject(responseProd.asString());
        Assert.assertEquals(jsonObjectQA.get("statusCode"), jsonObjectProd.get("statusCode"));
        Assert.assertEquals(jsonObjectQA.get("statusMessage"), jsonObjectProd.get("statusMessage"));
        JSONArray listOfCitiesQA = (JSONArray) jsonObjectQA.get("listOfDepartureCity");
        JSONArray listOfCitiesPROD = (JSONArray) jsonObjectProd.get("listOfDepartureCity");
        Assert.assertEquals(listOfCitiesQA.length(), listOfCitiesPROD.length());
        boolean cityPresentIn = false;
        mapper = new ObjectMapper();
        /*this cityPresentIn flag is for keeping track if CityList 1 is present in 2 or not
        suppose city list 1 = 1,2 & list2 = 1,2,4 --count is same but we need to track exact elements in both lists too.*/
        for (Object cityDetailsObject : listOfCitiesPROD) {
            CityDetailsRequest cityDetailprod = mapper.readValue(cityDetailsObject.toString(), CityDetailsRequest.class);
            cityPresentIn = false;
            for (Object cityDetails1QAObject : listOfCitiesQA) {
                CityDetailsRequest cityDetailsPojo1QA = mapper
                        .readValue(cityDetails1QAObject.toString(), CityDetailsRequest.class);
                logger.info("Checking for City " + cityDetailsPojo1QA.getName());
                if (cityDetailprod.getId().equals(cityDetailsPojo1QA.getId())) {
                    Assert.assertTrue(cityDetailsPojo1QA.equals(cityDetailprod));
                    cityPresentIn = true;
                    logger.info(
                            "Comparison Done " + cityDetailsPojo1QA.getName() + " with " + cityDetailprod.getName());
                    break;
                }
                cityPresentIn = false;
            }
            Assert.assertTrue(cityPresentIn);
        }

    }

    @Override public Object populateDataInJSONBody(List<List<String>> attributes) {
        return null;
    }

}
