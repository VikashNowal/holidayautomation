package com.mmt.automation.api.test.stepdefinitions;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import com.mmt.automation.api.test.parsers.common.CommonParsers;

import com.mmt.automation.test.common.helperservices.APIParserObjectMaker;
import com.mmt.automation.test.common.interfaces.APIParsersBaseClass;
import com.mmt.automation.test.common.Enums.Environment;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.json.simple.parser.ParseException;
import org.apache.commons.lang3.StringUtils;
import org.testng.Assert;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.mmt.automation.test.common.helperservices.APIHelper;
import com.mmt.automation.test.common.utils.PropertiesHelper;
import cucumber.api.DataTable;
import cucumber.api.java.en.And;
import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;
import io.restassured.http.Header;
import io.restassured.http.Headers;

public class APIHandlingStepDef extends APIHelper {

    private static final Logger logger = LogManager.getLogger(APIHandlingStepDef.class);
    private final PropertiesHelper webPropHelper = PropertiesHelper.getInstance();
    private final CommonParsers commonParsers = CommonParsers.getInstance();

    @Given("^\"(.*?)\" sets the following headers attributes for the endpoint \"(.*?)\"$") public void setAttriubtesForEndpoint(
            String userType, String endPointName, DataTable attributesTable) {
        String strHeader = null;
        Header hdr = null;
        headerlist = new ArrayList<Header>();
        try {
            item = attributesTable.raw();
            for (int j = 1; j < item.size(); j++) {
                if (item.get(j).get(1).contains("$")) {
                    strHeader = item.get(j).get(1).replace("$", " ").trim();
                    strHeader = webPropHelper.getTestDataProperty(strHeader);
                } else {
                    strHeader = item.get(j).get(1);
                }
                hdr = new Header(item.get(j).get(0), strHeader);
                headerlist.add(hdr);
            }
            // if(userType.contains("etail"))
            // {
            // hdr = new Header("Authorization", "Bearer "+getLoginSDTokenValue());
            // headerlist.add(hdr);
            // }
            reqHeaders = new Headers(headerlist);
        } catch (Exception e) {
            logger.error(getAPIFailedstep(endPointName));
        }

    }

    @When("^\"(.*?)\" makes a POST call for base url \"(.*?)\" using \"(.*?)\" with the body \"(.*?)\" for \"(.*?)\"$") public void makesPOSTCallWithThebody(
            String userType, String baseUrl, String paramInfo, String inputJsonFileName, String endPoint,
            DataTable attributesTable) {
        apiBaseUrl = webPropHelper.getConfigPropProperty(baseUrl); // Read from test data file
        endPointValue = webPropHelper.getEndpointProProperty(endPoint);
        attributes = attributesTable.raw();
        String endPointFilterValued = endPointFilteredValue(endPointValue, 1);
        endPointFilterValued = endPointFilterValued.replaceAll(filterPattern, "/");
        restURI = apiBaseUrl + endPointFilterValued;
        logger.info(endPointV + restURI);
        response = initiatePostRestAPICall(restURI, inputJsonFileName);
        logger.info(response.asString());
    }

    @When("^\"(.*?)\" makes a POST call for base url \"(.*?)\" using \"(.*?)\" without body for \"(.*?)\"$") public void makesPOSTCallWithoutThebody(
            String userType, String baseUrl, String paramInfo, String endPoint, DataTable attributesTable) {
        apiBaseUrl = webPropHelper.getConfigPropProperty(baseUrl); // Read from test data file
        endPointValue = webPropHelper.getEndpointProProperty(endPoint);
        attributes = attributesTable.raw();
        String endPointFilterValued = endPointFilteredValue(endPointValue, 1);
        endPointFilterValued = endPointFilterValued.replaceAll(filterPattern, "/");
        restURI = apiBaseUrl + endPointFilterValued;
        logger.info(endPointV + restURI);
        response = initiatePostRestAPICall(restURI);
        logger.info(response.asString());
    }

    @When("^\"(.*?)\" makes a POST call for base url \"(.*?)\" using \"(.*?)\" with the body \"(.*?)\" for endpoint \"(.*?)\" on \"(.*?)\" environment$") public void makesPOSTCallWithThebodyAndEnvironment(
            String userType, String baseUrl, String paramInfo, String inputJsonFileName, String endPoint,
            String environment, DataTable attributesTable) {
        apiBaseUrl = webPropHelper.getConfigPropProperty(baseUrl); // Read from test data file
        endPointValue = webPropHelper.getEndpointProProperty(endPoint);
        attributes = attributesTable.raw();
        String endPointFilterValued = endPointFilteredValue(endPointValue, 1);
        endPointFilterValued = endPointFilterValued.replaceAll(filterPattern, "/");
        restURI = apiBaseUrl + endPointFilterValued;
        if (environment.toString().equalsIgnoreCase("STAGING_QA")) {
           response= initiatePostRestAPICallForMultiEnvironment(restURI, inputJsonFileName,
                    Environment.STAGING_QA);
        } else if (environment.toString().equalsIgnoreCase("PRODUCTION")) {
           response= initiatePostRestAPICallForMultiEnvironment(restURI, inputJsonFileName,
                    Environment.PRODUCTION);
        } else {
            logger.info(" INVALID ENVIRONMENT SEND");
        }
    }

    /**
     * use this when we need to send RequestBody as Class Object instead of JSON or
     * string
     */

    @When("^\"(.*?)\" makes a POST call for base url \"(.*?)\" and RequestBody \"(.*?)\" for endPoint \"(.*?)\"$") public void makes_a_POST_call_for_base_url_and_RequestBody_for_endPoint(
            String userType, String baseUrl, String requestClassName, String endPoint, DataTable attributesTable)
            throws Throwable {
        {
            apiBaseUrl = webPropHelper.getConfigPropProperty(baseUrl); // Read from test data file
            endPointValue = webPropHelper.getEndpointProProperty(endPoint);
            attributes = attributesTable.raw();
            apiBaseUrl = webPropHelper.getConfigPropProperty(baseUrl); // Read from test data file
            endPointValue = webPropHelper.getEndpointProProperty(endPoint);
            attributes = attributesTable.raw();
            APIParsersBaseClass apiParserObject = APIParserObjectMaker.getClassObjectOfAPI(requestClassName);
            Object requestObject = apiParserObject.populateDataInJSONBody(attributes);
            String endPointFilterValued = endPointFilteredValue(endPointValue, 1);
            endPointFilterValued = endPointFilterValued.replaceAll(filterPattern, "/");
            restURI = apiBaseUrl + endPointFilterValued;
            logger.info(endPointV + restURI);
            logger.info(getRequestBodyAsJson(requestObject));
            response = initiateDynamicPostApiWithPOJO(restURI, requestObject);
            logger.info(response.asString());
        }
    }

    @When("^\"(.*?)\" makes a POST call for base url \"(.*?)\" and RequestBody \"(.*?)\" for endPoint \"(.*?)\" on \"(.*?)\" environment$") public void hitPostCallwithPojoClassBodyOnSpecificEnvironment(
            String userType, String baseUrl, String requestClassName, String endPoint, String environment,
            DataTable attributesTable) throws Throwable {
        {
            apiBaseUrl = webPropHelper.getConfigPropProperty(baseUrl); // Read from test data file
            endPointValue = webPropHelper.getEndpointProProperty(endPoint);
            attributes = attributesTable.raw();
            APIParsersBaseClass apiParserObject = APIParserObjectMaker.getClassObjectOfAPI(requestClassName);
            Object requestObject = apiParserObject.populateDataInJSONBody(attributes);
            String endPointFilterValued = endPointFilteredValue(endPointValue, 1);
            endPointFilterValued = endPointFilterValued.replaceAll(filterPattern, "/");
            restURI = apiBaseUrl + endPointFilterValued;
            logger.info(endPointV + restURI);
            logger.info(getRequestBodyAsJson(requestObject));
            if (environment.toString().equalsIgnoreCase("STAGING_QA")) {
                initiateDynamicPostApiWithPOJOOnEnvironment(restURI, requestObject,
                        Environment.STAGING_QA);
            } else if (environment.toString().equalsIgnoreCase("PRODUCTION")) {
                initiateDynamicPostApiWithPOJOOnEnvironment(restURI, requestObject,
                        Environment.PRODUCTION);
            } else {
                Assert.fail("FAILED due to INVALID ENVIRONMENT ");
            }
        }
    }

    @Given("^\"(.*?)\" makes a get call for base url \"(.*?)\" with \"(.*?)\" for endpoint \"(.*?)\" on \"(.*?)\" environment$") public void makesGetCallWith(
            String userType, String baseUrl, String paramInfo, String endPoint, String environment,
            DataTable attributesTable) {
        apiBaseUrl = webPropHelper.getConfigPropProperty(baseUrl);
        endPointValue = webPropHelper.getEndpointProProperty(endPoint);
        attributes = attributesTable.raw();
        String endPointFilterValued = endPointFilteredValue(endPointValue, 1);
        endPointFilterValued = endPointFilterValued.replaceAll(filterPattern, "/");
        restURI = apiBaseUrl + endPointFilterValued;
        if (environment.toString().equalsIgnoreCase("STAGING_QA")) {
            initiateRestGetAPICallForMultiEnvironment(restURI, Environment.STAGING_QA);
        } else if (environment.toString().equalsIgnoreCase("PRODUCTION"))
            initiateRestGetAPICallForMultiEnvironment(restURI, Environment.PRODUCTION);
        else {
            logger.info(" INVALID ENVIRONMENT SEND");
        }
    }

    @Given("^\"(.*?)\" makes a get call for base url \"(.*?)\" with \"(.*?)\" for \"(.*?)\"$") public void makesGetCall(
            String userType, String baseUrl, String paramInfo, String endPoint, DataTable attributesTable) {
        apiBaseUrl = webPropHelper.getConfigPropProperty(baseUrl);
        endPointValue = webPropHelper.getEndpointProProperty(endPoint);
        attributes = attributesTable.raw();
        String endPointFilterValued = endPointFilteredValue(endPointValue, 1);
        endPointFilterValued = endPointFilterValued.replaceAll(filterPattern, "/");
        restURI = apiBaseUrl + endPointFilterValued;
        initiateRestGetAPICall(restURI);
        logger.info(response.asString());
    }

    @When("^\"(.*?)\" makes a DELETE call with \"(.*?)\" for \"(.*?)\"$") public void makesDeleteCallWithAttributes(
            String userType, String endpoint, String paramInfo, String appType, DataTable attributesTable) {
        apiBaseUrl = webPropHelper.getConfigPropProperty("APIBaseURL");
        endPointValue = webPropHelper.getEndpointProProperty(endpoint);
        attributes = attributesTable.raw();
        String endPointFilterValued = endPointFilteredValue(endPointValue, 1);
        endPointFilterValued = endPointFilterValued.replaceAll(filterPattern, "/");
        restURI = apiBaseUrl + endPointFilterValued;
        logger.info(endPointV + restURI);
        initiateRestDeleteAPICall(restURI);
        logger.info(response.asString());
        // setSDApiResponse(response.asString());
    }

    @When("^\"(.*?)\" makes a PUT call using \"(.*?)\" with the body \"(.*?)\" for \"(.*?)\"$") public void makesPutTCallWithThebody(
            String userType, String endPoint, String paramInfo, String inputJsonFileName, String appType,
            DataTable attributesTable) {
        apiBaseUrl = webPropHelper.getConfigPropProperty("APIBaseURL");
        endPointValue = webPropHelper.getEndpointProProperty(endPoint);
        attributes = attributesTable.raw();
        String endPointFilterValued = endPointFilteredValue(endPointValue, 1);
        endPointFilterValued = endPointFilterValued.replaceAll(filterPattern, "/");
        restURI = apiBaseUrl + endPointFilterValued;
        logger.info(endPointV + restURI);
        response = initiatePutRestAPICall(restURI, inputJsonFileName);
        logger.info(response.asString());
    }

    @When("^\"(.*?)\" makes a PUT call using \"(.*?)\" without the body for \"(.*?)\"$") public void makesPutTCallWithoutThebody(
            String userType, String endPoint, String paramInfo, String appType, DataTable attributesTable) {
        apiBaseUrl = webPropHelper.getConfigPropProperty("APIBaseURL");
        endPointValue = webPropHelper.getEndpointProProperty(endPoint);
        attributes = attributesTable.raw();
        String endPointFilterValued = endPointFilteredValue(endPointValue, 1);
        endPointFilterValued = endPointFilterValued.replaceAll(filterPattern, "/");
        restURI = apiBaseUrl + endPointFilterValued;
        logger.info(endPointV + restURI);
        response = initiatePutRestAPICall(restURI);
        logger.info(response.asString());
        // setSDApiResponse(response.asString());
    }

    @Then("^verify \"(.*?)\" recieves status code as$") public void verifyRecievesStatusCode(String retailUnit,
            DataTable attributesTable) {
        try {
            int statusCodeExpected;
            int currentStatusCode = response.getStatusCode();
            attributes = attributesTable.raw();

            statusCodeExpected = Integer.parseInt(attributes.get(1).get(0));
            if (currentStatusCode != statusCodeExpected) {
                logger.error(getAPIFailedstep(
                        "Status code not matching. Exepected->" + statusCodeExpected + " != " + currentStatusCode + "\n"
                                + response.asString()));

            }
            getAPIPassedstep(
                    "Actual Status Code is matching with expected- " + statusCodeExpected + "--" + currentStatusCode);

        } catch (Exception e) {
            logger.error(getAPIFailedstep(e + retailUnit) + response.asString());
        }
    }

    @Then("^\"(.*?)\" verifies \"(.*?)\" from the above response$") public void verifiesKeyValuesFromResponse(
            String userType, String infoMsg, DataTable attributesTable) {
        String key = null;
        ArrayList<String> actualValueList = new ArrayList<>();
        ArrayList<String> expectedValueList = new ArrayList<>();
        ObjectMapper objectMapper = new ObjectMapper();
        try {
            String strResponse = response.asString();
            JsonNode root = objectMapper.readTree(strResponse);
            attributes = attributesTable.raw();

            for (int i = 1; i < attributes.size(); i++) {

                key = attributes.get(i).get(0);
                expectedValueList.add(attributes.get(i).get(1));
                actualValueList = APIHelper.attributeMatchingRecursive(root, key, actualValueList);
                if (!actualValueList.isEmpty()) {
                    getAPIPassedstep(attributes.get(1).get(0));
                } else {
                    getAPIFailedstep(attributes.get(1).get(0));
                }

                Assert.assertEquals(actualValueList, expectedValueList);
                logger.info(actualList + expectedValueList);
                logger.info(expectedList + actualValueList);
            }

        } catch (Exception e) {

            logger.error(getAPIFailedstep(key));
        }

    }

    @And("^validate that the attributes below are matching in \"(.*?)\" response$") public void validateTheAttributeBelowMatchingResponse(
            String endPointName, DataTable attributesTable) throws IOException {
        ObjectMapper objectMapper = new ObjectMapper();
        ArrayList<String> aryLst = new ArrayList<>();
        String strResponse = response.asString();
        JsonNode root = objectMapper.readTree(strResponse);
        attributes = attributesTable.raw();
        int i = 0;
        try {
            for (List<String> myItem : attributes) {
                if (i != 0) {
                    aryLst = APIHelper.attributeMatchingRecursive(root, myItem.get(0).trim(), aryLst);
                    if (!aryLst.isEmpty()) {
                        getAPIPassedstep(myItem.get(0));
                    } else {
                        getAPIFailedstep(myItem.get(0));
                    }
                }
                i++;
            }
            for (String lst : aryLst) {
                logger.info(lst);
            }
        } catch (Exception e) {
            getAPIFailedstep(endPointName);

        }
    }

    @Then("^\"(.*?)\" save the response for below attributes$") public void saveResponseAttributes(String userType,
            DataTable attributesTable) {
        ArrayList<String> attributesList = new ArrayList<>();
        try {
            ObjectMapper objectMapper = new ObjectMapper();
            JsonNode root = objectMapper.readTree(response.asString());
            attributes = attributesTable.raw();
            attributesList.clear();
            //            String value = "";
            for (int i = 1; i < attributes.size(); i++) {
                attributesList = APIHelper.attributeMatchingRecursive(root, attributes.get(i).get(0));
                dynamicValueMap.put(attributes.get(i).get(0), attributesList);
                //                  value = APIHelper.attributeMatchingRecursiveSingleValue(root, attributes.get(i).get(0), value);
                //                  dynamicValueMap.put(attributes.get(i).get(0), value);
            }

        } catch (Exception e) {
            logger.error(getAPIFailedstep(e.toString()));
        }
    }

    @Then("^\"(.*?)\" assert the response for below attributes$") public void assertResponseAttributes(String userType,
            DataTable attributesTable) {
        attributes = attributesTable.raw();
        String attribute = null;
        int j = 0;
        ArrayList<String> expectedAttributesList = new ArrayList<>();
        for (j = 1; j < attributes.size(); j++) {

            if (attributes.get(j).get(0).contains("$")) {
                attribute = attributes.get(j).get(0).replace("$", " ").trim();
                attribute = webPropHelper.getTestDataProperty(attribute);
                expectedAttributesList.add(attribute);
            } else {
                attribute = attributes.get(j).get(0).trim();
                expectedAttributesList.add(attribute);
            }
        }
        Assert.assertEquals(actualAttributesList, expectedAttributesList);
        logger.info(actualList + expectedAttributesList);
        logger.info(expectedList + actualAttributesList);
    }

    @Then("^Save the \"(.*?)\" from the response$") public void getsNewlyAddedRecordId(String childNode) {
        try {
            dynamicValue = APIHelper.getValueFromJSon(response.asString(), childNode);
            logger.info(dynamicValue);
        } catch (Exception e) {
            logger.error(getAPIFailedstep(dynamicValue));
        }
    }

    @Then("^\"(.*?)\" create a dynamic request payload for \"(.*?)\"$") public void create_a_dynamic_request_payload_for(
            String userType, String jsonRequestFilePath, DataTable table) throws Throwable {
        try {
            createDynamicRequest(userType, jsonRequestFilePath, table);
        } catch (Exception e) {
            logger.error(getAPIFailedstep(jsonRequestFilePath));
        }
    }

    @Then("^Store the \"(.*?)\" from the response$") public void store_the_from_the_response(String arg1) {
        dynamicValue = commonParsers.getDynamicPackageId(response.asString());
    }

    @And("^\"(.*?)\" verifes following attributes values are not Null from \"(.*?)\" response$") public void validateNotNull(
            String userType, String msg, DataTable attributesTable) {
        try {
            ObjectMapper objectMapper = new ObjectMapper();
            String strResponse = response.asString();
            JsonNode root = objectMapper.readTree(strResponse);
            attributes = attributesTable.raw();
            ArrayList<String> aryLst = new ArrayList<>();
            String valueCheck = null;
            int i;
            int j;
            for (i = 1; i < attributes.size(); i++) {
                aryLst = attributeMatchingRecursive(root, attributes.get(i).get(0), aryLst);

                for (j = 0; j < aryLst.size(); j++) {

                    valueCheck = aryLst.get(j);
                    if (valueCheck == null) {
                        getAPIFailedstep(attributes.get(i).get(0));
                    }
                }
            }
        } catch (Exception e) {
            getAPIFailedstep(userType);
        }
    }

    @Then("^Compare if response on prod versus Testing Environment are same for API \"(.*?)\"$") public void compareAPITwoResponseOnProductionVersusQA(
            String APIname) throws IOException, ParseException {
        logger.info("comparing Two Response Objects  on Test versus Prod");
        APIParsersBaseClass apiParserObject = APIParserObjectMaker.getClassObjectOfAPI(APIname);
        apiParserObject.compareQAversusProdResponse(responseStaging, responseProd);
    }

    @Given("^\"(.*?)\" sets body with following payLoads$") public void setBodyForEndpoint(String userType,
            DataTable attributesTable) {
        try {
            item = attributesTable.raw();
            String jsonBody = item.get(1).get(1);

            if (jsonBody.startsWith("$")) {
                jsonBody = webPropHelper.getTestDataProperty(jsonBody);
            } else {
                jsonBody = "{";
                headerlist = new ArrayList<>();
                String key;
                String value;
                String rKey = "key";
                String rValue = "value";
                String dataType;
                String temp = null;

                for (int i = 1; i < item.size(); i++) {
                    key = item.get(i).get(0);
                    value = item.get(i).get(1);
                    jsonBody = jsonBody + "\"key\":";
                    jsonBody = jsonBody.replaceAll(rKey, key);
                    if (value.contains("newRecord")) {
                        value = dynamicValue;
                    }
                    dataType = item.get(i).get(2);
                    if (!dataType.contains("int") && !dataType.contains("bool")) {
                        temp = "\"value\"";
                        temp = temp.replaceAll(rValue, value);
                        value = temp;
                    }
                    jsonBody = jsonBody + value + ",";
                }
                jsonBody = StringUtils.chop(jsonBody);
                globalPayLoads = jsonBody + "}";
            }
        } catch (Exception e) {
            logger.error(getAPIFailedstep(userType));
        }
    }

}




