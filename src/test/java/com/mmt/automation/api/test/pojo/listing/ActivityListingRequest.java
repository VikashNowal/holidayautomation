package com.mmt.automation.api.test.pojo.listing;

public class ActivityListingRequest {
    private String channel;
    private String website;
    private String funnel;
    private String affiliate;
    private String lob;
    private String dynamicPackageId;
    private int staySequence;

    public void setChannel(String channel) {
        this.channel = channel;
    }

    public String getChannel() {
        return this.channel;
    }

    public void setWebsite(String website) {
        this.website = website;
    }

    public String getWebsite() {
        return this.website;
    }

    public void setFunnel(String funnel) {
        this.funnel = funnel;
    }

    public String getFunnel() {
        return this.funnel;
    }

    public void setAffiliate(String affiliate) {
        this.affiliate = affiliate;
    }

    public String getAffiliate() {
        return this.affiliate;
    }

    public void setLob(String lob) {
        this.lob = lob;
    }

    public String getLob() {
        return this.lob;
    }

    public void setDynamicPackageId(String dynamicPackageId) {
        this.dynamicPackageId = dynamicPackageId;
    }

    public String getDynamicPackageId() {
        return this.dynamicPackageId;
    }

    public void setStaySequence(int staySequence) {
        this.staySequence = staySequence;
    }

    public int getStaySequence() {
        return this.staySequence;
    }
}
