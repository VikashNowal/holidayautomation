package com.mmt.automation.api.test.parsers.common;

import com.mmt.automation.test.common.helperservices.CommonActionUIHelper;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;

public class CommonParsers {

    private static final Logger logger = LogManager.getLogger(CommonParsers.class);
    private static CommonParsers commonParsersObj = null;
    private String getDynamicPackageId = null;
    JSONParser parser;

    public static CommonParsers getInstance() {
        if (commonParsersObj == null)
            commonParsersObj = new CommonParsers();
        return commonParsersObj;
    }

    public String getDynamicPackageId(String responseBody) {
        getDynamicPackageId = null;
        try {
            parser = new JSONParser();
            JSONObject packageDetailResponse = (JSONObject) parser.parse(responseBody);
            JSONObject packageDetailJsonObj = (JSONObject) packageDetailResponse.get("packageDetail");
            getDynamicPackageId = (String) packageDetailJsonObj.get("dynamicId");
        } catch (Exception e) {
            logger.error(CommonActionUIHelper.getAPIFailedstep(e.toString()));
        }
        return getDynamicPackageId;
    }

}
