package com.mmt.automation.api.test.parsers.details;

import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.mmt.automation.api.test.parsers.common.CommonParsers;
import com.mmt.automation.api.test.pojo.details.CancellationPolicyRequest;
import com.mmt.automation.test.common.interfaces.APIParsersBaseClass;

import io.restassured.response.Response;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;

import com.mmt.automation.test.common.helperservices.CommonActionUIHelper;
import org.testng.Assert;

public class CancellationPolicyParser implements APIParsersBaseClass {

    private static final Logger logger = LogManager.getLogger(CommonParsers.class);
    private static CancellationPolicyParser cancellationPolicyParserObj = null;
    JSONParser parser = new JSONParser();
    ObjectMapper mapper = new ObjectMapper();

    public static CancellationPolicyParser getInstance() {
        if (cancellationPolicyParserObj == null)
            cancellationPolicyParserObj = new CancellationPolicyParser();
        return cancellationPolicyParserObj;
    }

    public boolean getPenaltyAmountComparison(String response) {
        List<JSONObject> PenaltyAmountJSONObjList = new ArrayList<JSONObject>();
        JSONObject jsonObj;
        boolean result = false;
        try {
            jsonObj = (JSONObject) parser.parse(response);
            JSONObject penaltyDetailJsonObj = (JSONObject) jsonObj.get("penaltyDetail");
            JSONObject cancellationPenaltyJsonObj = (JSONObject) penaltyDetailJsonObj.get("cancellationPenalty");
            JSONArray penaltiesJsonArray = (JSONArray) cancellationPenaltyJsonObj.get("penalties");

            for (Object obj : penaltiesJsonArray) {
                PenaltyAmountJSONObjList.add((JSONObject) obj);
            }
            Long firstPenalty = (Long) PenaltyAmountJSONObjList.get(0).get("penalty");
            Long secondPenalty = (Long) PenaltyAmountJSONObjList.get(1).get("penalty");
            result = (firstPenalty < secondPenalty ? true : false);

        } catch (ParseException e) {
            logger.error(CommonActionUIHelper.getAPIFailedstep(e.toString()));
        }
        return result;
    }

    public boolean getPenaltyDateComparison(String response) throws java.text.ParseException {
        List<JSONObject> PenaltyDateJSONObjList = new ArrayList<JSONObject>();
        SimpleDateFormat sdfo = new SimpleDateFormat("yyyy-MM-dd");
        JSONObject jsonObj;
        boolean result = false;
        try {
            jsonObj = (JSONObject) parser.parse(response);
            JSONObject penaltyDetailJsonObj = (JSONObject) jsonObj.get("penaltyDetail");
            JSONObject cancellationPenaltyJsonObj = (JSONObject) penaltyDetailJsonObj.get("cancellationPenalty");
            JSONArray penaltiesJsonArray = (JSONArray) cancellationPenaltyJsonObj.get("penalties");

            for (Object obj : penaltiesJsonArray) {
                PenaltyDateJSONObjList.add((JSONObject) obj);
            }
            Date firstPenaltyDate = sdfo.parse((String) PenaltyDateJSONObjList.get(0).get("fromDate"));
            Date secondPenaltyDate = sdfo.parse((String) PenaltyDateJSONObjList.get(1).get("fromDate"));
            result = (firstPenaltyDate.compareTo(secondPenaltyDate) < 0 ? true : false);

        } catch (ParseException e) {
            logger.error(CommonActionUIHelper.getAPIFailedstep(e.toString()));
        }
        return result;
    }

    public boolean getNonRefundableValue(String response, int packagePrice) throws java.text.ParseException {
        JSONObject LastPenaltyAmountJSONObj;
        JSONObject jsonObj;
        boolean result = false;
        int i;
        Long penalty = null;
        try {
            jsonObj = (JSONObject) parser.parse(response);
            JSONObject penaltyDetailJsonObj = (JSONObject) jsonObj.get("penaltyDetail");
            JSONObject cancellationPenaltyJsonObj = (JSONObject) penaltyDetailJsonObj.get("cancellationPenalty");
            JSONArray penaltiesJsonArray = (JSONArray) cancellationPenaltyJsonObj.get("penalties");

            LastPenaltyAmountJSONObj = (JSONObject) penaltiesJsonArray.get(penaltiesJsonArray.size() - 1);
            penalty = (Long) LastPenaltyAmountJSONObj.get("penalty");
            result = (penalty == packagePrice ? true : false);

        } catch (ParseException e) {
            logger.error(CommonActionUIHelper.getAPIFailedstep(e.toString()));
        }
        return result;
    }

    public String getPackageDetailPrice(String response, String key) throws ParseException {
        JSONObject jsonObj = (JSONObject) parser.parse(response);
        JSONObject pricingDetailJsonObj = (JSONObject) jsonObj.get("pricingDetail");
        JSONArray categoryPricesArrayObj = (JSONArray) pricingDetailJsonObj.get("categoryPrices");
        JSONObject categoryPricesJsonObj = (JSONObject) categoryPricesArrayObj.get(0);
        key = (String) categoryPricesJsonObj.get("key");
        return key;
    }

   /* public void compareCancellationDetailResponse(Response responseProd, Response responseStaging)
            throws IOException, ParseException {


    }*/

    @Override public void compareQAversusProdResponse(Response responseQA, Response responseProd)
            throws IOException, ParseException {
        JSONObject jsonObjectQA = (JSONObject) parser.parse(responseQA.asString());
        JSONObject jsonObjectProd = (JSONObject) parser.parse(responseProd.asString());

        Assert.assertEquals(jsonObjectQA.get("statusCode"), jsonObjectProd.get("statusCode"));
        Assert.assertEquals(jsonObjectQA.get("statusMessage"), jsonObjectProd.get("statusMessage"));

        JSONObject penaltyDetailJsonObjQa = (JSONObject) jsonObjectQA.get("penaltyDetail");
        JSONObject cancellationPenaltyJsonObjQa = (JSONObject) penaltyDetailJsonObjQa.get("cancellationPenalty");
        JSONArray listOfPenaltiesQa = (JSONArray) cancellationPenaltyJsonObjQa.get("penalties");

        JSONObject penaltyDetailJsonObjProd = (JSONObject) jsonObjectProd.get("penaltyDetail");
        JSONObject cancellationPenaltyJsonObjProd = (JSONObject) penaltyDetailJsonObjProd.get("cancellationPenalty");
        JSONArray listOfPenaltiesProd = (JSONArray) cancellationPenaltyJsonObjProd.get("penalties");

        Assert.assertEquals(listOfPenaltiesQa.size(), listOfPenaltiesProd.size());

        boolean dateMatches = false;

        for (Object penaltyObjectProd : listOfPenaltiesProd) {
            CancellationPolicyRequest cancellationPolicyPojoProd = mapper
                    .readValue(penaltyObjectProd.toString(), CancellationPolicyRequest.class);
            dateMatches = false;
            for (Object penaltyObjectQa : listOfPenaltiesQa) {
                CancellationPolicyRequest cancellationPolicyPojoQa = mapper
                        .readValue(penaltyObjectQa.toString(), CancellationPolicyRequest.class);
                if (cancellationPolicyPojoProd.getFromDate().equals(cancellationPolicyPojoQa.getFromDate())) {
                    Assert.assertTrue(cancellationPolicyPojoQa.equals(cancellationPolicyPojoProd));
                    dateMatches = true;
                    break;
                }
                dateMatches = false;
            }
            Assert.assertTrue(dateMatches);
        }
    }

    @Override public Object populateDataInJSONBody(List<List<String>> attributes) {
        return null;
    }

}

