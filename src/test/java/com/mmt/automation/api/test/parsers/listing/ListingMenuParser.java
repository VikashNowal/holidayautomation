package com.mmt.automation.api.test.parsers.listing;

import com.mmt.automation.test.common.helperservices.APIHelper;
import com.mmt.automation.test.common.interfaces.APIParsersBaseClass;
import io.restassured.response.Response;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.json.simple.parser.ParseException;

import java.io.IOException;
import java.util.List;

public class ListingMenuParser implements APIParsersBaseClass {

    private static final Logger logger = LogManager.getLogger(ListingMetaParser.class);
    private static ListingMenuParser listingMenuParser = null;

    public static ListingMenuParser getInstance() {
        if (listingMenuParser == null)
            listingMenuParser = new ListingMenuParser();

        return listingMenuParser;
    }

    @Override public void compareQAversusProdResponse(Response responseQA, Response responseProd)
            throws IOException, ParseException {
        APIHelper.assertingProdVersusQAResponse(responseProd, responseQA);
    }

    @Override public Object populateDataInJSONBody(List<List<String>> attributes) {
        return null;
    }
}