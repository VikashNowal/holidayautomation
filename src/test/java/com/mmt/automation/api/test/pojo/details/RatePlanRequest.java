package com.mmt.automation.api.test.pojo.details;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import javax.annotation.Generated;
import com.fasterxml.jackson.annotation.JsonAnyGetter;
import com.fasterxml.jackson.annotation.JsonAnySetter;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({
        "code",
        "vendor",
        "mealCode",
        "mealName",
        "mealsIncluded"
})
@Generated("jsonschema2pojo")
public class RatePlanRequest {

    @JsonProperty("code")
    private String code;
    @JsonProperty("vendor")
    private String vendor;
    @JsonProperty("mealCode")
    private String mealCode;
    @JsonProperty("mealName")
    private String mealName;
    @JsonProperty("mealsIncluded")
    private List<String> mealsIncluded = null;
    @JsonIgnore
    private Map<String, Object> additionalProperties = new HashMap<String, Object>();

    @JsonProperty("code")
    public String getCode() {
        return code;
    }

    @JsonProperty("code")
    public void setCode(String code) {
        this.code = code;
    }

    @JsonProperty("vendor")
    public String getVendor() {
        return vendor;
    }

    @JsonProperty("vendor")
    public void setVendor(String vendor) {
        this.vendor = vendor;
    }

    @JsonProperty("mealCode")
    public String getMealCode() {
        return mealCode;
    }

    @JsonProperty("mealCode")
    public void setMealCode(String mealCode) {
        this.mealCode = mealCode;
    }

    @JsonProperty("mealName")
    public String getMealName() {
        return mealName;
    }

    @JsonProperty("mealName")
    public void setMealName(String mealName) {
        this.mealName = mealName;
    }

    @JsonProperty("mealsIncluded")
    public List<String> getMealsIncluded() {
        return mealsIncluded;
    }

    @JsonProperty("mealsIncluded")
    public void setMealsIncluded(List<String> mealsIncluded) {
        this.mealsIncluded = mealsIncluded;
    }

    @JsonAnyGetter
    public Map<String, Object> getAdditionalProperties() {
        return this.additionalProperties;
    }

    @JsonAnySetter
    public void setAdditionalProperty(String name, Object value) {
        this.additionalProperties.put(name, value);
    }

}