package com.mmt.automation.api.test.stepdefinitions;

import com.mmt.automation.api.test.parsers.details.CancellationPolicyParser;
import com.mmt.automation.test.common.helperservices.APIHelper;
import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.json.simple.parser.ParseException;
import org.testng.Assert;

import java.io.IOException;
import java.util.ArrayList;

public class CancellationDetailsStepDef extends APIHelper {
    private static final Logger logger = LogManager.getLogger(APIHandlingStepDef.class);
    private final CancellationPolicyParser cancellationPolicyParser = CancellationPolicyParser.getInstance();

    @Given("^\"(.*?)\" compare the first penalty amount should be less than equal to second penalty amount$") public void compare_the_first_penalty_amount_should_be_less_than_equal_to_second_penalty_amount(
            String userType) {

        Assert.assertTrue(cancellationPolicyParser.getPenaltyAmountComparison(response.asString()));
    }

    @Given("^\"(.*?)\" compare the first penalty date should be before the second penalty date$") public void compare_the_first_penalty_date_should_be_before_the_second_penalty_date(
            String userType) {

        Assert.assertTrue(cancellationPolicyParser.getPenaltyAmountComparison(response.asString()));
    }

    @Given("^\"(.*?)\" verify that Non Refundable should be TRUE when Penalty amount is equal to package price$") public void verify_that_Non_Refundable_should_be_TRUE_when_Package_price_is_equal_to_package_price(
            String userType) throws java.text.ParseException {
        int discountPriceAmount = Integer
                .parseInt((String) ((ArrayList) dynamicValueMap.get("discountedPrice")).get(0));
        int serviceTaxAmount = Integer.parseInt((String) ((ArrayList) dynamicValueMap.get("serviceTax")).get(0));
        int packagePrice = discountPriceAmount * 2 + serviceTaxAmount * 2;
        Assert.assertTrue(cancellationPolicyParser.getNonRefundableValue(response.asString(), packagePrice));
    }

    @Then("^Compare if cancellation policy response on prod versus Testing Environment are same$") public void compareTwoResponseOnProductionVersusQAEnvironment()
            throws IOException, ParseException {
        logger.info("comparing Two Objects of Cancellation Penalties on Test versus Prod");
        cancellationPolicyParser.compareQAversusProdResponse(responseProd, responseStaging);
    }

}
