package com.mmt.automation.api.test.pojo.details;

import java.util.List;

public class RoomRequest {
    private int noOfAdults;
    private int noOfChildrenWB;
    private List<Object> listOfAgeOfChildrenWB;

    public int getNoOfAdults() {
        return noOfAdults;
    }

    public void setNoOfAdults(int noOfAdults) {
        this.noOfAdults = noOfAdults;
    }

    public int getNoOfChildrenWB() {
        return noOfChildrenWB;
    }

    public void setNoOfChildrenWB(int noOfChildrenWB) {
        this.noOfChildrenWB = noOfChildrenWB;
    }

    public List<Object> getListOfAgeOfChildrenWB() {
        return listOfAgeOfChildrenWB;
    }

    public void setListOfAgeOfChildrenWB(List<Object> listOfAgeOfChildrenWB) {
        this.listOfAgeOfChildrenWB = listOfAgeOfChildrenWB;
    }

}
