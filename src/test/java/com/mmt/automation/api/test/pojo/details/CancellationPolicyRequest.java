package com.mmt.automation.api.test.pojo.details;

import java.util.Objects;

import com.fasterxml.jackson.annotation.JsonProperty;

public class CancellationPolicyRequest {

    @JsonProperty("fromDate")
    private String fromDate;
    @JsonProperty("penalty")
    private Integer penalty;
    @JsonProperty("nonRefundable")
    private Boolean nonRefundable;
    @JsonProperty("withZCPenalty")
    private Integer withZCPenalty;

    @JsonProperty("fromDate")
    public String getFromDate() {
        return fromDate;
    }

    @JsonProperty("fromDate")
    public void setFromDate(String fromDate) {
        this.fromDate = fromDate;
    }

    @JsonProperty("penalty")
    public Integer getPenalty() {
        return penalty;
    }

    @JsonProperty("penalty")
    public void setPenalty(Integer penalty) {
        this.penalty = penalty;
    }

    @JsonProperty("nonRefundable")
    public Boolean getNonRefundable() {
        return nonRefundable;
    }

    @JsonProperty("nonRefundable")
    public void setNonRefundable(Boolean nonRefundable) {
        this.nonRefundable = nonRefundable;
    }

    @JsonProperty("withZCPenalty")
    public Integer getWithZCPenalty() {
        return withZCPenalty;
    }

    @JsonProperty("withZCPenalty")
    public void setWithZCPenalty(Integer withZCPenalty) {
        this.withZCPenalty = withZCPenalty;
    }

    @Override public boolean equals(Object o) {
        if (this == o)
            return true;
        if (!(o instanceof CancellationPolicyRequest))
            return false;
        CancellationPolicyRequest that = (CancellationPolicyRequest) o;
        return Objects.equals(getFromDate(), that.getFromDate()) && Objects.equals(getPenalty(), that.getPenalty()) && Objects
                .equals(getNonRefundable(), that.getNonRefundable()) && Objects.equals(getWithZCPenalty(), that.getWithZCPenalty());
    }

    @Override public int hashCode() {
        return Objects.hash(getFromDate(), getPenalty(), getNonRefundable(), getWithZCPenalty());
    }


}
