package com.mmt.automation.api.test.parsers.details;

import com.mmt.automation.api.test.pojo.details.DynamicDetailsRequest;
import com.mmt.automation.api.test.pojo.details.RoomRequest;
import com.mmt.automation.test.common.interfaces.APIParsersBaseClass;
import io.restassured.response.Response;
import org.json.JSONArray;
import org.json.JSONObject;
import org.json.simple.parser.ParseException;
import org.testng.Assert;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class DynamicDetailParser implements APIParsersBaseClass {

    private static DynamicDetailParser dynamicDetailParser = null;
    private DynamicDetailsRequest request;
    private RoomRequest room1;
    private static Map<Integer, String> cityDayItineraryMap = new HashMap<>();
    public static Map<String, Response> mapResponse = new HashMap<>();

    public static DynamicDetailParser getInstance() {
        if (dynamicDetailParser == null)
            dynamicDetailParser = new DynamicDetailParser();
        return dynamicDetailParser;
    }

    @SuppressWarnings("unchecked") public boolean checkComponentNodePresent(Response response, String componentName) {
        boolean inclusionFlag = false;

        switch (componentName) {
        case "flights":
            if (response.jsonPath().get("packageDetail.flightDetail") != null)
                inclusionFlag = true;
            break;
        case "hotels":
            if (response.jsonPath().get("packageDetail.hotelDetail") != null)
                inclusionFlag = true;
            break;
        case "meals":

            if (isBundledPackage(response).equals("true")) {
                if (response.jsonPath().get("packageDetail.mealDetail") != null)
                    inclusionFlag = true;
            } else if (isBundledPackage(response).equals("false")) {
                try {
                    List<Object> hotelCategory = response.jsonPath()
                            .get("packageDetail.hotelDetail.hotelCategoryDetails");
                    Map<Object, Object> hotels = (Map<Object, Object>) hotelCategory.get(0);
                    List<Object> hotelList = (List<Object>) hotels.get("hotels");

                    for (Object object : hotelList) {
                        Map<Object, Object> hotelObject = (Map<Object, Object>) object;
                        List<Object> roomTypeListhotelList = (List<Object>) hotelObject.get("roomTypes");

                        for (Object roomType : roomTypeListhotelList) {
                            Map<Object, Object> roomTypeMap = (Map<Object, Object>) roomType;
                            Map<Object, Object> rateplanList = (Map<Object, Object>) roomTypeMap.get("ratePlan");
                            List<String> mealsIncluded = (List<String>) rateplanList.get("mealsIncluded");
                            for (String meals : mealsIncluded) {
                                if (meals.contains("BREAKFAST") || meals.contains("DINNER")) {
                                    inclusionFlag = true;
                                }
                            }
                        }
                    }
                } catch (ClassCastException e) {
                    e.printStackTrace();
                }
            }
            break;
        case "carItinerary":
            if (response.jsonPath().get("packageDetail.carItineraryDetail") != null)
                inclusionFlag = true;
            break;
        case "cabItinerary":
            if (response.jsonPath().get("packageDetail.cabItineraryDetail") != null)
                inclusionFlag = true;
            break;
        /*case "cityDrops":
            if (response.jsonPath().get("packageDetail.mealDetail")!=null);
            inclusionFlag=true;*/
        case "activities":
            if (response.jsonPath().get("packageDetail.activityDetail") != null)
                inclusionFlag = true;
            break;
        case "airportTransfers":
            if (response.jsonPath().get("packageDetail.transferDetail") != null)
                inclusionFlag = true;
            break;
        case "visa":
            if (response.jsonPath().get("packageDetail.visaDetail") != null)
                inclusionFlag = true;
            break;
        default:
            break;
        }
        return inclusionFlag;
    }

    public String isBundledPackage(Response response) {
        return (String.valueOf(response.jsonPath().get("packageDetail.metadataDetail.bundled")));
    }

    public String getPackageBranch(Response response) {
        return (response.jsonPath().get("packageDetail.metadataDetail.branch"));
    }

    public String getFlightDetailsType(Response response) {
        return (response.jsonPath().get("packageDetail.flightDetail.flightDetailType"));

    }

    public void verifyTransferComponents(Response response) {

        Assert.assertNotNull(response.jsonPath().get("packageDetail.transferDetail"));
        Assert.assertEquals(response.jsonPath().get("packageDetail.transferDetail.component"), "AIRPORT_TRANSFER");

        JSONObject jsonObj = new JSONObject(response.asString());
        JSONObject packageDetail = (JSONObject) jsonObj.get("packageDetail");
        JSONObject transferDetails = (JSONObject) packageDetail.get("transferDetail");
        JSONArray airportTransfersObjects = (JSONArray) transferDetails.get("airportTransfers");

        for (Object airPortTransferObject : airportTransfersObjects) {
            JSONObject tranferObject = (JSONObject) airPortTransferObject;
            Assert.assertNotNull(tranferObject.get("airportTransferType"));
            Assert.assertTrue((tranferObject.get("airportTransferType").equals("AIRPORT_TO_HOTEL") || (
                    tranferObject.get("airportTransferType").equals("HOTEL_TO_AIRPORT") || (tranferObject
                            .get("airportTransferType").equals("HOTEL_TO_HOTEL")))));
            Assert.assertTrue(
                    tranferObject.get("defaultSelection").equals("PRIVATE") || tranferObject.get("defaultSelection")
                            .equals("SHARED"));
            if (tranferObject.get("defaultSelection").equals("PRIVATE")) {
                JSONObject privateTransfer = (JSONObject) tranferObject.get("privateTransfer");
                Assert.assertNotNull(privateTransfer.get("sellableId"));
                Assert.assertNotNull(privateTransfer.get("recheckKey"));
                Assert.assertNotNull(privateTransfer.get("tourGrade"));
                Assert.assertNotNull(privateTransfer.get("activityVendorData"));
                Assert.assertNotNull(privateTransfer.get("date"));
            } else if (tranferObject.get("defaultSelection").equals("SHARED")) {
                JSONObject groupTransfer = (JSONObject) tranferObject.get("groupTransfer");
                Assert.assertNotNull(groupTransfer.get("sellableId"));
                Assert.assertNotNull(groupTransfer.get("recheckKey"));
                Assert.assertNotNull(groupTransfer.get("tourGrade"));
                Assert.assertNotNull(groupTransfer.get("activityVendorData"));
                Assert.assertNotNull(groupTransfer.get("date"));
            } else {
                Assert.fail("Invalid Transfers TYPE");
            }

        }
    }

    public void checkVisaComponentDetails(Response response) {
        JSONObject jsonObj = new JSONObject(response.asString());
        JSONObject packageDetail = (JSONObject) jsonObj.get("packageDetail");
        JSONObject visaDetails = (JSONObject) packageDetail.get("visaDetail");
        Assert.assertEquals(visaDetails.get("component"), "VISA");
        Assert.assertNotNull(visaDetails.get("countryVisaDetailMap"));
        JSONArray visas = (JSONArray) visaDetails.get("visas");
        for (Object obj : visas) {
            JSONObject visa = (JSONObject) obj;
            Assert.assertNotNull(visa.get("sellableId"));
            Assert.assertEquals(visa.get("visaType"), "INCLUDED");
            Assert.assertNotNull(visa.get("country"));
            Assert.assertNotNull(visa.get("visaTypeV2"));
        }
    }

    public void assertFlightComponents(Response response, String type) {
        if (type.equals("DOM_RETURN")) {
            Assert.assertNotNull(response.jsonPath().get("packageDetail.flightDetail.departureFlight.sellableId"));
            Assert.assertNotNull(response.jsonPath().get("packageDetail.flightDetail.departureFlight.sellableId"));
            Assert.assertNotNull(response.jsonPath().get("packageDetail.flightDetail.returnFlight.sellableId"));
            Assert.assertNotNull(response.jsonPath().get("packageDetail.flightDetail.returnFlight.flightId"));
            Assert.assertNotNull(response.jsonPath().get("packageDetail.flightDetail.returnFlight.flightKey"));
            int return_duration = response.jsonPath().get("packageDetail.flightDetail.returnFlight.duration");
            int onward_duration = response.jsonPath().get("packageDetail.flightDetail.departureFlight.duration");
            Assert.assertTrue(return_duration > 0);
            Assert.assertTrue(onward_duration > 0);
            Assert.assertNotNull(response.jsonPath().get("packageDetail.flightDetail.returnFlight.flightLegs"));
            Assert.assertNotNull(response.jsonPath().get("packageDetail.flightDetail.returnFlight.flightReviewKey"));
            Assert.assertNotNull(response.jsonPath().get("packageDetail.flightDetail.returnFlight.rFareCombKey"));
            Assert.assertNotNull(response.jsonPath().get("packageDetail.flightDetail.departureFlight.flightLegs"));
            Assert.assertNotNull(response.jsonPath().get("packageDetail.flightDetail.departureFlight.flightReviewKey"));
            Assert.assertNotNull(response.jsonPath().get("packageDetail.flightDetail.departureFlight.rFareCombKey"));
        }
        if (type.equals("DOM_ONWARDS")) {
            Assert.assertNotNull(response.jsonPath().get("packageDetail.flightDetail.flightGroup"));
            Assert.assertNotNull(response.jsonPath().get("packageDetail.flightDetail.flightGroup.flightReviewKey"));
            Assert.assertNotNull(response.jsonPath().get("packageDetail.flightDetail.flightGroup.flightKey"));
            List<Object> flights = response.jsonPath().get("packageDetail.flightDetail.flightGroup.flights");
            Assert.assertTrue(flights.size() > 1);
            for (Object flight : flights) {
                Map<Object, Object> flightObject = (Map<Object, Object>) flight;
                Assert.assertEquals(flightObject.get("flightType"), "INTERMEDIATE");
                Assert.assertNotNull(flightObject.get("sellableId"));
            }
        }
        if (type.equals("OBT")) {
            Assert.assertNotNull(response.jsonPath().get("packageDetail.flightDetail"));
            Map<Object, Object> flightDataMap = response.jsonPath().get("packageDetail.flightDetail.obtFlightGroup");
            Assert.assertNotNull(flightDataMap.get("sellableId"));
            Assert.assertNotNull(flightDataMap.get("flightReviewKey"));
            List<Object> flights = (List<Object>) flightDataMap.get("flights");
            for (Object flight : flights) {
                Assert.assertTrue(flights.size() > 1);
                Map<Object, Object> flightMap = (Map<Object, Object>) flight;
                Assert.assertNotNull(flightMap.get("sellableId"));
                Assert.assertNotNull(flightMap.get("flightId"));
                Assert.assertTrue(Integer.parseInt(String.valueOf(flightMap.get("duration"))) > 0);
                Assert.assertNotNull(flightMap.get("departure"));
                Assert.assertNotNull(flightMap.get("arrival"));
            }
        }
    }

    public void assertHotelComponent(Response response) {

        Map<Object, Object> hotelCategory = response.jsonPath()
                .getMap("packageDetail.hotelDetail.hotelCategoryDetails[0]");

        List<Object> hotelList = (List<Object>) hotelCategory.get("hotels");

        for (Object hotels : hotelList) {
            Map<Object, Object> hotelMaps = (Map<Object, Object>) hotels;
            Assert.assertNotNull(hotelMaps.get("sellableId"));
            Assert.assertNotNull(hotelMaps.get("hotelCode"));
            Assert.assertNotNull(hotelMaps.get("roomTypes"));
            Assert.assertNotNull(hotelMaps.get("componentCostPrice"));
            Assert.assertNotNull(hotelMaps.get("componentCommission"));
            Assert.assertNotNull(hotelMaps.get("staySequence"));
            Assert.assertNotNull(hotelMaps.get("checkInDate"));
            Assert.assertNotNull(hotelMaps.get("checkOutDate"));
            if (isBundledPackage(response).equals("true")) {
                Assert.assertEquals(hotelMaps.get("bundled"), true);
            } else {
                Assert.assertEquals(hotelMaps.get("bundled"), false);
            }
        }
    }

    public Map<String, String> getInclusionMap(Response response) {
        return response.jsonPath().get("packageDetail.packageInclusionsDetail");
    }

    public void verifyInclusionMaps(Map<String, String> inclusionsMap, Response response) {
        for (String componentName : inclusionsMap.keySet()) {
            if (String.valueOf(inclusionsMap.get(componentName)).equals("true")) {
                Assert.assertTrue(checkComponentNodePresent(response, componentName));
            }
        }
    }

    public void verifyCouponSection(Response response) {
        if (response.jsonPath().get("packageDetail.pricingDetail.categoryPrices[0].couponDiscount") != null) {
            Assert.assertNotNull(
                    response.jsonPath().get("packageDetail.pricingDetail.categoryPrices[0].couponDiscount.couponCode"));
        }
    }

    public void verifyPriceSection(Response response) {
        int couponAmount = 0;
        if (response.jsonPath().get("packageDetail.pricingDetail.categoryPrices[0]") != null) {
            int price = response.jsonPath().get("packageDetail.pricingDetail.categoryPrices[0].price");
            int pax = response.jsonPath().get("packageDetail.pricingDetail.categoryPrices[0].paxMultiplier");
            int discountedprice = response.jsonPath()
                    .get("packageDetail.pricingDetail.categoryPrices[0].discountedPrice");
            if (response.jsonPath().get("packageDetail.pricingDetail.categoryPrices[0].couponDiscount") != null) {
                couponAmount = response.jsonPath()
                        .get("packageDetail.pricingDetail.categoryPrices[0].couponDiscount.amount.price");
            }
            Assert.assertEquals(Math.abs((price - discountedprice) * pax), Math.abs(couponAmount));
        }
    }

    public void checkActivitiesComponentDetails(Response response) {

        JSONObject packageResponse = new JSONObject(response.asString());
        JSONObject packageDetailObject = (JSONObject) packageResponse.get("packageDetail");
        JSONObject activityDetailObject = (JSONObject) packageDetailObject.get("activityDetail");

        Assert.assertEquals(activityDetailObject.get("component"), "ACTIVITY");
        Assert.assertNotNull(activityDetailObject.get("cityActivities"));

        JSONArray cityActivityDetails = (JSONArray) activityDetailObject.get("cityActivities");

        for (Object object : cityActivityDetails) {
            JSONObject cityActivityDetail = (JSONObject) object;

            Assert.assertNotNull(cityActivityDetail.get("cityCode"));
            JSONArray activitiesArray = (JSONArray) cityActivityDetail.get("activities");
            for (Object activityObject : activitiesArray) {

                JSONObject activity = (JSONObject) activityObject;

                Assert.assertNotNull(activity.get("sellableId"));
                Assert.assertNotNull(activity.get("recheckKey"));
                Assert.assertNotNull(activity.get("tourGrade"));
                Assert.assertNotNull(activity.get("date"));
                Assert.assertNotNull(activity.get("metaData"));
                Assert.assertNotNull(activity.get("imageDetail"));
                Assert.assertNotNull(activity.get("additionalData"));
                Assert.assertNotNull(activity.get("activityVendorData"));
            }

        }

    }

    @Override public void compareQAversusProdResponse(Response responseQA, Response responseProd)
            throws IOException, ParseException {
        //Todo
    }

    @Override public Object populateDataInJSONBody(List<List<String>> attributes) {
        try {
            request = new DynamicDetailsRequest();
            request.setChannel(attributes.get(4).get(1));
            request.setDepartureCity(attributes.get(5).get(1));
            request.setWebsite(attributes.get(1).get(1));
            request.setPackageId(Integer.parseInt(attributes.get(3).get(1)));
            request.setTravelDate(attributes.get(2).get(1));
            room1 = new RoomRequest();
            room1.setNoOfAdults(Integer.parseInt(
                    attributes.get(6).get(1)));     //TODO:  need to make this dynamic Basic data in feature file
            room1.setListOfAgeOfChildrenWB(new ArrayList<>());
            List<RoomRequest> listRoom = new ArrayList<>();
            listRoom.add(room1);
            request.setRooms(listRoom);

            return request;
        } catch (Exception e) {
            return null;
        }
    }

    public Map<Integer, String> getDayWiseCityDetailsMap(Response response) {
        JSONObject packageResponse = new JSONObject(response.asString());
        JSONObject packageDetailObject = (JSONObject) packageResponse.get("packageDetail");
        JSONObject destinationDetail = (JSONObject) packageDetailObject.get("destinationDetail");
        JSONArray destinationsObjects = destinationDetail.getJSONArray("destinations");
        for (Object object : destinationsObjects) {
            JSONObject dayItinerary = (JSONObject) object;
            cityDayItineraryMap.put((Integer) dayItinerary.get("staySequence"), dayItinerary.getString("name"));
        }
        return cityDayItineraryMap;
    }

    public void storeResponseInMap(String name, Response response) {
        mapResponse.put(name, response);
    }
}
