package com.mmt.automation.api.test.pojo.listing;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import javax.annotation.Generated;
import com.fasterxml.jackson.annotation.JsonAnyGetter;
import com.fasterxml.jackson.annotation.JsonAnySetter;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({
        "channel",
        "website",
        "funnel",
        "affiliate",
        "lob",
        "departureCity",
        "destinationCity",
        "criterias",
        "offset",
        "sorterCriterias",
        "packageIds"
})
@Generated("jsonschema2pojo")
public class ListingPackageRequest {

    @JsonProperty("channel")
    private String channel;
    @JsonProperty("website")
    private String website;
    @JsonProperty("funnel")
    private String funnel;
    @JsonProperty("affiliate")
    private String affiliate;
    @JsonProperty("lob")
    private String lob;
    @JsonProperty("departureCity")
    private String departureCity;
    @JsonProperty("destinationCity")
    private String destinationCity;
    @JsonProperty("criterias")
    private List<Object> criterias = null;
    @JsonProperty("offset")
    private Integer offset;
    @JsonProperty("sorterCriterias")
    private List<SorterCriteriaRequest> sorterCriteriaRequests = null;
    @JsonProperty("packageIds")
    private List<Object> packageIds = null;
    @JsonIgnore
    private Map<String, Object> additionalProperties = new HashMap<String, Object>();

    @JsonProperty("channel")
    public String getChannel() {
        return channel;
    }

    @JsonProperty("channel")
    public void setChannel(String channel) {
        this.channel = channel;
    }

    @JsonProperty("website")
    public String getWebsite() {
        return website;
    }

    @JsonProperty("website")
    public void setWebsite(String website) {
        this.website = website;
    }

    @JsonProperty("funnel")
    public String getFunnel() {
        return funnel;
    }

    @JsonProperty("funnel")
    public void setFunnel(String funnel) {
        this.funnel = funnel;
    }

    @JsonProperty("affiliate")
    public String getAffiliate() {
        return affiliate;
    }

    @JsonProperty("affiliate")
    public void setAffiliate(String affiliate) {
        this.affiliate = affiliate;
    }

    @JsonProperty("lob")
    public String getLob() {
        return lob;
    }

    @JsonProperty("lob")
    public void setLob(String lob) {
        this.lob = lob;
    }

    @JsonProperty("departureCity")
    public String getDepartureCity() {
        return departureCity;
    }

    @JsonProperty("departureCity")
    public void setDepartureCity(String departureCity) {
        this.departureCity = departureCity;
    }

    @JsonProperty("destinationCity")
    public String getDestinationCity() {
        return destinationCity;
    }

    @JsonProperty("destinationCity")
    public void setDestinationCity(String destinationCity) {
        this.destinationCity = destinationCity;
    }

    @JsonProperty("criterias")
    public List<Object> getCriterias() {
        return criterias;
    }

    @JsonProperty("criterias")
    public void setCriterias(List<Object> criterias) {
        this.criterias = criterias;
    }

    @JsonProperty("offset")
    public Integer getOffset() {
        return offset;
    }

    @JsonProperty("offset")
    public void setOffset(Integer offset) {
        this.offset = offset;
    }

    @JsonProperty("sorterCriterias")
    public List<SorterCriteriaRequest> getSorterCriteriaRequests() {
        return sorterCriteriaRequests;
    }

    @JsonProperty("sorterCriterias")
    public void setSorterCriteriaRequests(List<SorterCriteriaRequest> sorterCriteriaRequests) {
        this.sorterCriteriaRequests = sorterCriteriaRequests;
    }

    @JsonProperty("packageIds")
    public List<Object> getPackageIds() {
        return packageIds;
    }

    @JsonProperty("packageIds")
    public void setPackageIds(List<Object> packageIds) {
        this.packageIds = packageIds;
    }

    @JsonAnyGetter
    public Map<String, Object> getAdditionalProperties() {
        return this.additionalProperties;
    }

    @JsonAnySetter
    public void setAdditionalProperty(String name, Object value) {
        this.additionalProperties.put(name, value);
    }

}