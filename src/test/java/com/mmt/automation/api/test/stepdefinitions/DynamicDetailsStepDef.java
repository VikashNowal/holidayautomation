package com.mmt.automation.api.test.stepdefinitions;

import com.mmt.automation.api.test.parsers.details.DynamicDetailParser;
import com.mmt.automation.test.common.helperservices.APIHelper;
import com.mmt.automation.test.common.utils.PropertiesHelper;
import cucumber.api.java.en.And;
import io.restassured.response.Response;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.testng.Assert;

import java.util.HashMap;
import java.util.Map;

public class DynamicDetailsStepDef extends APIHelper {

    private static final Logger logger = LogManager.getLogger(DynamicDetailsStepDef.class);
    private PropertiesHelper webPropHelper = PropertiesHelper.getInstance();
    private DynamicDetailParser dynamicdetailParser = DynamicDetailParser.getInstance();

    @And("^verify inclusions Components are present in DetailResponse$") public void verify_inclusions_Components_are_present_in_DetailResponse() {
        Map<String, String> inclusionsMap = dynamicdetailParser.getInclusionMap(response);
        dynamicdetailParser.verifyInclusionMaps(inclusionsMap, response);
    }

    @And("^verify couponCode and coupon details section is Present$") public void verify_couponCode_and_coupon_details_section_is_Present() {
        logger.info("Asserting CouponCode and coupon Details ");
        dynamicdetailParser.verifyCouponSection(response);
        logger.info("Assertion Completed for  CouponCode and coupon Details ");
    }

    @And("^validate prices are correct in details Response$") public void validate_prices_are_correct_in_details_Response() {
        logger.info("Asserting Prices section ");
        dynamicdetailParser.verifyPriceSection(response);
        logger.info("Assertion Complete for Prices section ");
    }

    @SuppressWarnings("unchecked") @And("^verify hotel components in API response$") public void verify_hotel_components_in_API_response() {
        logger.info("Asserting Hotel Components in API repsonse");
        Assert.assertNotNull(response.jsonPath().get("packageDetail.hotelDetail"), "Hotel Component coming NULL");
        dynamicdetailParser.assertHotelComponent(response);
    }

    @SuppressWarnings("unchecked") @And("^verify flight components in API response$") public void verify_flight_components_in_API_response() {
        logger.info("Asserting Flight Components in API repsonse");
        if (!dynamicdetailParser.checkComponentNodePresent(response, "flights")) {
            logger.info(" No FLights included in this Package -> No Assertion Needed");
            Assert.assertTrue(true);
        } else {
            logger.info("Asserting Flight Component Present ");
            Assert.assertNotNull(response.jsonPath().get("packageDetail.flightDetail"), "Flight Component coming NULL");
            logger.info("Asserting if the Flight is DOM  corresponding nodes are available ");
            if (dynamicdetailParser.getPackageBranch(response).equals("DOM")) {
                if (dynamicdetailParser.getFlightDetailsType(response).equals("DOM_RETURN")) {
                    dynamicdetailParser.assertFlightComponents(response, "DOM_RETURN");
                } else if (dynamicdetailParser.getFlightDetailsType(response).equals("DOM_ONWARDS")) {
                    dynamicdetailParser.assertFlightComponents(response, "DOM_ONWARDS");
                } else
                    Assert.fail("Seems wrong Data coming ");
            } else if (dynamicdetailParser.getPackageBranch(response).equals("OBT")) {
                dynamicdetailParser.assertFlightComponents(response, "OBT");
            }
        }
        logger.info("Asserting Completed for Flight Components in API repsonse");
    }

    @And("^verify Transfers components in API response if included$") public void verify_Transfers_components_in_API_response_if_included() {
        logger.info("Asserting Transfer Component ");
        if (!dynamicdetailParser.checkComponentNodePresent(response, "airportTransfers")) {
            logger.info(" No Transfers  included in this Package -> No Assertion Needed");
            Assert.assertTrue(true);
            return;
        }
        dynamicdetailParser.verifyTransferComponents(response);
        logger.info(" Assertion Finished for Transfer");
    }

    @And("^verify Visa component details in API response if included$") public void verify_Visa_component_details_in_API_response_if_included() {
        if (!dynamicdetailParser.checkComponentNodePresent(response, "visa")) {
            logger.info(" No Visa  included in this Package -> No Assertion Needed");
            Assert.assertTrue(true);
            return;
        }
        dynamicdetailParser.checkVisaComponentDetails(response);
    }

    @And("^verify Activity component details in API response if included$") public void verify_Activity_component_details_in_API_response_if_included() {
        if (!dynamicdetailParser.checkComponentNodePresent(response, "activities")) {
            logger.info(" No Activities included in this Package -> No Assertion Needed");
            Assert.assertTrue(true);
            return;
        }
        dynamicdetailParser.checkActivitiesComponentDetails(response);
    }

    @And("^Store the Response of DynamicDetails with key \"(.*?)\"$")
    public void storeResponseOfAPI(String name)
    {
        dynamicdetailParser.storeResponseInMap(name,response);

    }

}


