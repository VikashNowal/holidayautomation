package com.mmt.automation.api.test.stepdefinitions;

import com.mmt.automation.api.test.parsers.details.ActivityListingParser;
import com.mmt.automation.test.common.helperservices.APIHelper;
import cucumber.api.DataTable;
import cucumber.api.java.en.And;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.json.simple.parser.ParseException;

public class ActivityChangeStepDef extends APIHelper {
    private static final Logger logger = LogManager.getLogger(ActivityChangeStepDef.class);
    private final ActivityListingParser activityListingParser = ActivityListingParser.getInstance();

    @And("verify package PriceMap is coming in response") public void compare_the_first_penalty_amount_should_be_less_than_equal_to_second_penalty_amount()
            throws ParseException {
        activityListingParser.verifyPriceMapSection(response);

    }

    @And("^verify activities cityDetails are as per Activity DaywiseCityMap$")
    public void verifyCityDetails(DataTable attributesTable ) throws ParseException {
        attributes = attributesTable.raw();
        activityListingParser.verifyCityDetails(response,Integer.parseInt(attributes.get(1).get(1)));
    }
}
