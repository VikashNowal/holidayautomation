package com.mmt.automation.api.test.parsers.listing;

import com.mmt.automation.test.common.helperservices.APIHelper;
import com.mmt.automation.test.common.interfaces.APIParsersBaseClass;
import io.restassured.response.Response;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.json.simple.parser.ParseException;

import java.io.IOException;
import java.util.List;


public class ListingMetaParser implements APIParsersBaseClass {

    private static final Logger logger = LogManager.getLogger(ListingMetaParser.class);
    private static ListingMetaParser listingMetaParser = null;


    public static ListingMetaParser getInstance() {
        if (listingMetaParser == null)
            listingMetaParser = new ListingMetaParser();

        return listingMetaParser;
    }

    @Override public void compareQAversusProdResponse(Response responseQA, Response responseProd)
            throws IOException, ParseException {
        APIHelper.assertingProdVersusQAResponse(responseProd,responseQA);
    }

    @Override public Object populateDataInJSONBody(List<List<String>> attributes) {
        return null;
    }
}
