package com.mmt.automation.api.test.parsers.details;

import com.mmt.automation.api.test.pojo.listing.ActivityListingRequest;
import com.mmt.automation.test.common.helperservices.APIHelper;
import com.mmt.automation.test.common.interfaces.APIParsersBaseClass;
import cucumber.api.java.sl.In;
import io.restassured.response.Response;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;
import org.testng.Assert;

import java.io.IOException;
import java.util.*;

public class ActivityListingParser implements APIParsersBaseClass {

    private static final Logger logger = LogManager.getLogger(ActivityListingParser.class);
    private static ActivityListingParser activityListingParser = null;
    private ActivityListingRequest activityListingRequest = new ActivityListingRequest();
    private JSONParser parser = new JSONParser();
    private Map<String, Integer> priceMap = new HashMap<>();
    private Set<String> activitiesCode = new HashSet<>();
    private JSONObject activityResponse, activityListingData, listingActivitiesObjects = new JSONObject();
    private JSONArray listingActivities = new JSONArray();
    private String recheckActivitiesCode;

    public static ActivityListingParser getInstance() {
        if (activityListingParser == null)
            activityListingParser = new ActivityListingParser();

        return activityListingParser;
    }

    @Override public void compareQAversusProdResponse(Response responseQA, Response responseProd)
            throws IOException, ParseException {

    }

    @Override public Object populateDataInJSONBody(List<List<String>> attributes) {

        activityListingRequest.setAffiliate(attributes.get(10).get(1));
        activityListingRequest.setChannel(attributes.get(4).get(1));
        activityListingRequest.setDynamicPackageId(APIHelper.dynamicValue);
        activityListingRequest.setFunnel(attributes.get(9).get(1));
        activityListingRequest.setLob(attributes.get(8).get(1));
        activityListingRequest.setStaySequence(Integer.parseInt(attributes.get(7).get(1)));
        activityListingRequest.setWebsite(attributes.get(1).get(1));
        return activityListingRequest;

    }

    public void verifyPriceMapSection(Response response) throws ParseException {

        activityResponse = (JSONObject) parser.parse(response.asString());
        Assert.assertNotNull(activityResponse.get("activityListingData"));
        activityListingData = (JSONObject) activityResponse.get("activityListingData");
        Assert.assertTrue((double) activityListingData.get("discountedFactor") > 0.0);
        HashMap<String, Integer> packagePriceMap = (JSONObject) activityListingData.get("packagePriceMap");
        JSONArray listingActivities = (JSONArray) activityListingData.get("listingActivities");
        activitiesCode = packagePriceMap.keySet();
        logger.info("verifying all the important nodes are not null in response");
        for (Object object : listingActivities) {
            listingActivitiesObjects = (JSONObject) object;
            Assert.assertNotNull(listingActivitiesObjects.get("componentCostPrice"));
            Assert.assertNotNull(listingActivitiesObjects.get("componentCommission"));
            Assert.assertNotNull(listingActivitiesObjects.get("recheckKey"));
            Assert.assertNotNull(listingActivitiesObjects.get("activityVendorData"));
            Assert.assertNotNull(listingActivitiesObjects.get("activityAvailabilityInfoList"));
            Assert.assertNotNull(listingActivitiesObjects.get("listingActivityMetaData"));
            recheckActivitiesCode = listingActivitiesObjects.get("recheckKey").toString().split("\\|")[0];
            logger.info("verifying that" + recheckActivitiesCode + " present in set of activities code in price map");
            Assert.assertTrue(activitiesCode.contains(recheckActivitiesCode));
        }
        Assert.assertTrue(packagePriceMap.size() > 0);
    }

    public void verifyCityDetails(Response response, Integer staySequence) throws ParseException {
        activityResponse = (JSONObject) parser.parse(response.asString());
        activityListingData = (JSONObject) activityResponse.get("activityListingData");
        JSONArray listingActivities = (JSONArray) activityListingData.get("listingActivities");
        Response dynamicDetailsResponse = DynamicDetailParser.mapResponse.get("dynamicDetailsResponse");
        Map<Integer, String> cityDetailsMap = DynamicDetailParser.getInstance()
                .getDayWiseCityDetailsMap(dynamicDetailsResponse);
        String stayCityForStaySequence = cityDetailsMap.get(staySequence);
        for (Object object : listingActivities) {
            listingActivitiesObjects = (JSONObject) object;
            JSONObject metaDateObject = (JSONObject) listingActivitiesObjects.get("metaData");
            String cityName = (String) metaDateObject.get("cityName");
            logger.info("StaySequence City " + stayCityForStaySequence + " -- "
                    + "Activities Data Cities for that sequence " + cityName);
        }

    }
}
