package com.mmt.automation.api.test.stepdefinitions;

import com.mmt.automation.test.common.helperservices.APIHelper;
import com.mmt.automation.test.common.helperservices.CommonActionUIHelper;
import com.mmt.automation.test.common.utils.WebDriverHelper;
import cucumber.api.Scenario;
import cucumber.api.java.After;
import cucumber.api.java.Before;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

public class Hooks {

    private static final Logger logger = LogManager.getLogger(com.mmt.automation.ui.test.stepdefinitions.Hooks.class);
    private final WebDriverHelper webDriverHelperObj = WebDriverHelper.getInstance();
    private final CommonActionUIHelper commonActionHelperObj = CommonActionUIHelper.getInstance();
    private Scenario scenario;

    @Before public void beforeSteps(Scenario scenario) {
        this.scenario = scenario;
        logger.info("BEFORE Test called");
        logger.debug("##################Execution Started For Scenario Name: " + scenario.getName()
                + "#######################");
        getTestType();
    }

    @After public void afterSteps() {
        logger.debug(
                "####################Execution End Scenario Name: " + scenario.getName() + "  :: Status:" + scenario
                        .getStatus() + "##############################");
        logger.info("AFTER Test called");
        APIHelper.reqHeaders = null;
    }

    private String getTestType() {
        String testType = "";
        for (String tag : scenario.getSourceTagNames()) {
            if (tag != null) {
                tag = tag.toLowerCase();
                if (tag.contains("api") || tag.contains("web") || tag.contains("mobile")) {
                    testType = tag.replaceAll("[^A-Za-z0-9-_]", "");
                    break;
                }
            }
        }
        return testType;
    }

}