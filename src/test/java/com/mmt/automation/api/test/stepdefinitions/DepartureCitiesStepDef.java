package com.mmt.automation.api.test.stepdefinitions;

import com.mmt.automation.api.test.parsers.listing.DepartureCitiesParser;
import com.mmt.automation.test.common.helperservices.APIHelper;
import com.mmt.automation.test.common.utils.PropertiesHelper;
import cucumber.api.java.en.Then;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.io.IOException;

public class DepartureCitiesStepDef extends APIHelper {

    private static final Logger logger = LogManager
            .getLogger(com.mmt.automation.api.test.stepdefinitions.DynamicDetailsStepDef.class);
    private PropertiesHelper webPropHelper = PropertiesHelper.getInstance();
    private DepartureCitiesParser departureCitiesParser = DepartureCitiesParser.getInstance();

    @Then("^Verify that the DepartureCities repsonse have listOfDepartureCities$") public void verify_that_the_DepartureCities_repsonse_have_listOfDepartureCities()
            throws IOException {
        logger.info("Asserting Departure city nodes");
        departureCitiesParser.validateDepeartureCitiesNodes(response);

    }

    @Then("^Compare if response on prod versus Testing Environment are same$") public void compareTwoResponseOnProductionVersusQAEnvironment()
            throws IOException {
        logger.info("comparing Two Objects of Departure cities on Test versus Prod");
        departureCitiesParser.compareQAversusProdResponse(responseProd, responseProd);

    }

}




