package com.mmt.automation.api.test.pojo.listing;

import java.util.HashMap;
import java.util.Map;
import java.util.Objects;

import com.fasterxml.jackson.annotation.JsonAnyGetter;
import com.fasterxml.jackson.annotation.JsonAnySetter;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;

public class CityDetailsRequest {

    @JsonProperty("id")
    private Integer id;
    @JsonProperty("name")
    private String name;
    @JsonProperty("countryName")
    private String countryName;
    @JsonProperty("countryId")
    private Integer countryId;
    @JsonProperty("topListed")
    private Boolean topListed;
    @JsonProperty("airportCode")
    private String airportCode;
    @JsonIgnore
    private Map<String, Object> additionalProperties = new HashMap<String, Object>();

    @JsonProperty("id")
    public Integer getId() {
        return id;
    }

    @JsonProperty("id")
    public void setId(Integer id) {
        this.id = id;
    }

    @JsonProperty("name")
    public String getName() {
        return name;
    }

    @JsonProperty("name")
    public void setName(String name) {
        this.name = name;
    }

    @JsonProperty("countryName")
    public String getCountryName() {
        return countryName;
    }

    @JsonProperty("countryName")
    public void setCountryName(String countryName) {
        this.countryName = countryName;
    }

    @JsonProperty("countryId")
    public Integer getCountryId() {
        return countryId;
    }

    @JsonProperty("countryId")
    public void setCountryId(Integer countryId) {
        this.countryId = countryId;
    }

    @JsonProperty("topListed")
    public Boolean getTopListed() {
        return topListed;
    }

    @JsonProperty("topListed")
    public void setTopListed(Boolean topListed) {
        this.topListed = topListed;
    }

    @JsonProperty("airportCode")
    public String getAirportCode() {
        return airportCode;
    }

    @JsonProperty("airportCode")
    public void setAirportCode(String airportCode) {
        this.airportCode = airportCode;
    }

    @JsonAnyGetter
    public Map<String, Object> getAdditionalProperties() {
        return this.additionalProperties;
    }

    @JsonAnySetter
    public void setAdditionalProperty(String name, Object value) {
        this.additionalProperties.put(name, value);
    }

    @Override public boolean equals(Object o) {
        if (this == o)
            return true;
        if (!(o instanceof CityDetailsRequest))
            return false;
        CityDetailsRequest that = (CityDetailsRequest) o;
        return Objects.equals(getId(), that.getId()) && Objects.equals(getName(), that.getName()) && Objects
                .equals(getCountryName(), that.getCountryName()) && Objects.equals(getCountryId(), that.getCountryId())
                && Objects.equals(getTopListed(), that.getTopListed()) && Objects
                .equals(getAirportCode(), that.getAirportCode()) && Objects
                .equals(getAdditionalProperties(), that.getAdditionalProperties());
    }

    @Override public int hashCode() {
        return Objects.hash(getId(), getName(), getCountryName(), getCountryId(), getTopListed(), getAirportCode(),
                getAdditionalProperties());
    }


}