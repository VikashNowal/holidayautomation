package com.mmt.automation.api.test.parsers.listing;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.mmt.automation.api.test.pojo.listing.ListingPackageRequest;
import com.mmt.automation.api.test.pojo.listing.SorterCriteriaRequest;
import com.mmt.automation.test.common.helperservices.APIHelper;
import com.mmt.automation.test.common.interfaces.APIParsersBaseClass;
import io.restassured.response.Response;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.json.JSONObject;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class ListingPackagesParser implements APIParsersBaseClass {

    private static final Logger logger = LogManager.getLogger(ListingPackagesParser.class);
    private static ListingPackagesParser listingPackagesParser = null;
    private SorterCriteriaRequest sorterCriteriaRequest = new SorterCriteriaRequest();
    private ListingPackageRequest request;
    private JSONObject parser;
    private ObjectMapper mapper;
    private List<Object> criteria = new ArrayList<>();
    private List<SorterCriteriaRequest> sorterCriteria = new ArrayList<>();
    private String criteria_String = "";
    List<String> values = new ArrayList<>();
    private List<Object> packageId = new ArrayList<>();

    public static ListingPackagesParser getInstance() {
        if (listingPackagesParser == null)
            listingPackagesParser = new ListingPackagesParser();

        return listingPackagesParser;
    }

    @Override public void compareQAversusProdResponse(Response responseQA, Response responseProd) throws IOException {
        APIHelper.assertingProdVersusQAResponse(responseQA, responseProd);
    }

    @Override public Object populateDataInJSONBody(List<List<String>> attributes) {
        try {
            request = new ListingPackageRequest();
            request.setChannel(attributes.get(4).get(1));
            request.setDepartureCity(attributes.get(5).get(1));
            request.setWebsite(attributes.get(1).get(1));
            request.setDestinationCity(attributes.get(6).get(1));
            request.setAffiliate(attributes.get(7).get(1));
            request.setCriterias(criteria);
            criteria_String = attributes.get(8).get(1);
            fillSorterCriteriaUsingInputString(criteria_String);
            request.setPackageIds(packageId);
            request.setSorterCriteriaRequests(sorterCriteria);
            request.setPackageIds(packageId);
            return request;
        } catch (Exception e) {
            logger.info(e.getStackTrace());
            return null;
        }
    }

    private void fillSorterCriteriaUsingInputString(String criteria_string) {
        String arr[] = criteria_string.split("_");
        SorterCriteriaRequest sorterCriteriaRequest = new SorterCriteriaRequest();
        sorterCriteriaRequest.setId(Integer.parseInt(arr[0]));
        for (int i = 1; i <arr.length; i++) {
            values.add(arr[i]);
        }
        sorterCriteriaRequest.setValues(values);
        sorterCriteria.add(sorterCriteriaRequest);
    }

}
