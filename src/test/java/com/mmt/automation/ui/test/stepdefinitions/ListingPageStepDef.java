package com.mmt.automation.ui.test.stepdefinitions;


import java.util.List;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.openqa.selenium.WebElement;
import org.testng.Assert;

import com.mmt.automation.test.common.helperservices.CommonActionUIHelper;

import cucumber.api.DataTable;
import cucumber.api.java.en.Then;

public class ListingPageStepDef {
	
	private final CommonActionUIHelper commonActionHelperObj = CommonActionUIHelper.getInstance();
	private static final Logger logger = LogManager.getLogger(ListingPageStepDef.class);
	private List<List<String>> elements=null;
	private String currentElement = null;
	private WebElement webElement;
	private String objPageName;
	private String pageName="ListingPo";
	
	@Then("^Click on the search button$")
	public void click_on_the_search_button(DataTable locatorType) {
		try {
			elements = locatorType.raw();
			for (int i = 1; i < elements.size(); i++) {
				currentElement = elements.get(i).get(0);
				objPageName = elements.get(i).get(1);
				if (!(objPageName.isEmpty())) {
					pageName = objPageName;
				}
				webElement = commonActionHelperObj.getBDDElement(currentElement,pageName);
//				Assert.assertTrue(commonActionHelperObj.scrollPageToWebElement(webElement));
				Assert.assertTrue(commonActionHelperObj.clickOnButton(webElement));
			}
		} catch (Exception e) {
			logger.error(CommonActionUIHelper.getFailedstep(currentElement));

		}
		
	}
	
	@Then("^Click on the live chat icon$")
	public void click_on_the_live_chat_icon(DataTable locatorType) {
		try {
			elements = locatorType.raw();
			for (int i = 1; i < elements.size(); i++) {
				currentElement = elements.get(i).get(0);
				objPageName = elements.get(i).get(1);
				if (!(objPageName.isEmpty())) {
					pageName = objPageName;
				}
				webElement = commonActionHelperObj.getBDDElement(currentElement,pageName);
//				Assert.assertTrue(commonActionHelperObj.scrollPageToWebElement(webElement));
				Assert.assertTrue(commonActionHelperObj.clickOnButton(webElement));
				Thread.sleep(6000);
			}
		} catch (Exception e) {
			logger.error(CommonActionUIHelper.getFailedstep(currentElement));

		}
		
	}

	
}
