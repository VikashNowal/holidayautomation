package com.mmt.automation.ui.test.stepdefinitions;


import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import com.mmt.automation.test.common.helperservices.CommonActionUIHelper;
import com.mmt.automation.test.common.utils.WebDriverHelper;

import cucumber.api.java.en.Given;


public class CommonStepDef {

	private final CommonActionUIHelper commonActionHelperObj = CommonActionUIHelper.getInstance();
	private final WebDriverHelper webDriverHelper = WebDriverHelper.getInstance();
	private static final Logger logger = LogManager.getLogger(CommonStepDef.class);

	@Given("^Open the \"(.*?)\"$")
	public void open_the_google(String applicationName) throws Throwable {

		webDriverHelper.open_web_driver(applicationName);

	}

}
