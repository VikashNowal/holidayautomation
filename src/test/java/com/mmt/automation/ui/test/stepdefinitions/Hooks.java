package com.mmt.automation.ui.test.stepdefinitions;

import java.io.File;
import java.io.IOException;

import com.mmt.automation.test.common.helperservices.CommonActionUIHelper;
import com.mmt.automation.test.common.utils.WebDriverHelper;
import org.apache.commons.io.FileUtils;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.openqa.selenium.TakesScreenshot;

import cucumber.api.Scenario;
import cucumber.api.java.After;
import cucumber.api.java.Before;
import org.openqa.selenium.OutputType;
import com.cucumber.listener.Reporter;

public class Hooks {

	private static final Logger logger = LogManager.getLogger(Hooks.class);
	private final WebDriverHelper webDriverHelperObj = WebDriverHelper.getInstance();
	private final CommonActionUIHelper commonActionHelperObj = CommonActionUIHelper.getInstance();
	private Scenario scenario;

	@Before
	public void beforeSteps(Scenario scenario) {
		this.scenario = scenario;
		logger.info("BEFORE Test called");
		logger.debug("*******************Execution Started For Scenario Name: " + scenario.getName() + "*********************");
	}

	@After(order = 1)
	public void afterScenario(Scenario scenario) {
		if (scenario.isFailed()) {
			String screenshotName = scenario.getName().replaceAll(" ", "_");
			try {
				File sourcePath = ((TakesScreenshot) webDriverHelperObj.driver).getScreenshotAs(OutputType.FILE);
				File destinationPath = new File(System.getProperty("user.dir") + "/target/cucumber-reports/screenshots/"
						+ screenshotName + ".png");
				FileUtils.copyFile(sourcePath, destinationPath);
				Reporter.addScreenCaptureFromPath(destinationPath.toString());
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
	}

	@After(order = 0)
	public void afterSteps() {
		try {
			logger.debug("Execution End For Scenario Name: " + scenario.getName() + "  :: Status:" + scenario.getStatus());

			logger.debug("Closing Webdriver or sessions.........");
			webDriverHelperObj.quitDriver();
			logger.debug("*************************Scenarios Execution End.*******************************");
			logger.info("AFTER Test called");
		} catch (Exception e) {
			logger.error(CommonActionUIHelper.getFailedstep(e));
		}
	}

}
