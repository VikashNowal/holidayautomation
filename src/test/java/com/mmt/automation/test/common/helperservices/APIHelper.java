package com.mmt.automation.test.common.helperservices;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.node.ArrayNode;
import com.mmt.automation.api.test.parsers.details.DynamicDetailParser;
import com.mmt.automation.test.common.Enums.Environment;
import com.mmt.automation.test.common.utils.PropertiesHelper;
import cucumber.api.DataTable;
import io.restassured.http.Header;
import io.restassured.http.Headers;
import io.restassured.response.Response;
import org.apache.commons.lang3.StringUtils;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.json.JSONObject;
import org.skyscreamer.jsonassert.JSONAssert;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.text.SimpleDateFormat;
import java.util.*;

import static io.restassured.RestAssured.given;

public class APIHelper extends CommonActionUIHelper {

    private static final Logger logger = LogManager.getLogger(CommonActionUIHelper.class);
    protected static Response response;
    public static Headers reqHeaders;
    protected String apiBaseUrl = null;

    public static String dynamicValue;
    public static Map<String, Object> dynamicValueMap = new HashMap<String, Object>();
    protected static List<List<String>> attributes;
    protected List<Header> headerlist;
    protected List<List<String>> item;
    protected String restURI = null;
    protected String globalPayLoads = null;
    protected ArrayList<String> actualAttributesList = new ArrayList<>();
    protected String filterPattern = "//";
    protected String endPointV = "Endpoint ->";
    protected String endPointValue;
    protected String actualList = "Actual List :";
    protected String expectedList = "Expected List :";
    protected String noRecordDelete = "No Record to Delete";
    private String dynamicRequestBody = null;
    protected static Response responseStaging;
    protected static Response responseProd;
    SimpleDateFormat formatter;
    Date date;
    long startTime;
    long valueTimeout = 100000;
    long diffTime;
    long currentTime;

    public static PropertiesHelper webPropHelper = PropertiesHelper.getInstance();
    public static DynamicDetailParser dynamicDetailParser = DynamicDetailParser.getInstance();

    public Response initiateRestGetAPICall(String url) {
        try {
            if (reqHeaders == null) {
                response = given().relaxedHTTPSValidation().when().get(url);
            } else {
                response = given().relaxedHTTPSValidation().headers(reqHeaders).when().get(url);
            }
            logger.debug("JSON Response::" + response.asString());

        } catch (Exception e) {
            response = null;
            logger.error("initiateRestAPICall exception msg::" + e.getMessage());
            e.printStackTrace();
        }
        return response;
    }

    public Response initiatePostRestAPICall(String url, String jsonRequestFilePath) {
        String apiBody = null;
        try {
            apiBody = convertJsonFileToString(
                    System.getProperty("user.dir") + "/src/test/resources/apiJsonRequests/" + jsonRequestFilePath
                            + ".json");
            apiBody = uniqueValueforPost(apiBody);
            if (reqHeaders == null) {
                response = given().relaxedHTTPSValidation().body(apiBody).post(url);
            } else {
                response = given().relaxedHTTPSValidation().headers(reqHeaders).body(apiBody).post(url);
            }

            logger.debug("JSON Response::" + response.asString());

        } catch (Exception e) {
            response = null;
            logger.error("initiateRestAPICall exception msg::" + e.getMessage());
        }
        return response;
    }

    public Response initiatePostRestAPICallForMultiEnvironment(String url, String jsonRequestFilePath,
            Environment environment) {
        String apiBody = null;
        try {
            apiBody = convertJsonFileToString(
                    System.getProperty("user.dir") + "/src/test/resources/apiJsonRequests/" + jsonRequestFilePath
                            + ".json");
            apiBody = uniqueValueforPost(apiBody);

            switch (environment.toString()) {
            case "STAGING_QA":
                if (reqHeaders == null) {
                    responseStaging = given().relaxedHTTPSValidation().body(apiBody).post(url);
                } else {
                    responseStaging = given().relaxedHTTPSValidation().headers(reqHeaders).body(apiBody).post(url);
                }
                logger.debug("JSON Response_STAGING/QA::" + responseStaging.asString());
                return responseStaging;

            case "PRODUCTION":
                if (reqHeaders == null) {
                    responseProd = given().relaxedHTTPSValidation().body(apiBody).post(url);
                } else {
                    responseProd = given().relaxedHTTPSValidation().headers(reqHeaders).body(apiBody).post(url);
                }
                logger.debug("JSON Response_STAGING/QA::" + responseProd.asString());
                return responseProd;

            default:
                break;
            }
        } catch (Exception e) {
            responseProd = null;
            responseStaging = null;
            logger.error("initiateRestAPICall exception msg::" + e.getMessage());
            e.printStackTrace();
        }
        return null;
    }

    public Response initiateRestGetAPICallForMultiEnvironment(String url, Environment environment) {
        try {
            if (environment.toString().equals("STAGING_QA")) {
                if (reqHeaders == null) {
                    responseStaging = given().relaxedHTTPSValidation().when().get(url);
                } else {
                    responseStaging = given().relaxedHTTPSValidation().headers(reqHeaders).when().get(url);
                }
                logger.debug("JSON Response_STAGING/QA::" + responseStaging.asString());
                response = responseStaging;
                return responseStaging;

            } else if (environment.toString().equals("PRODUCTION")) {
                if (reqHeaders == null) {
                    responseProd = given().relaxedHTTPSValidation().when().get(url);
                } else {
                    responseProd = given().relaxedHTTPSValidation().headers(reqHeaders).when().get(url);
                }
                response = responseProd;
                logger.debug("JSON Response Production::" + response.asString());
                return responseProd;
            }
        } catch (Exception e) {
            responseProd = null;
            responseStaging = null;
            logger.error("initiateRestAPICall exception msg::" + e.getMessage());
            e.printStackTrace();
        }
        return null;
    }

    public String createDynamicRequest(String userType, String jsonRequestFilePath, DataTable table) {
        dynamicRequestBody = convertJsonFileToString(
                System.getProperty("user.dir") + "/src/test/resources/apiJsonRequests/" + jsonRequestFilePath
                        + ".json");
        List<List<String>> elements = table.raw();
        try {
            int j = 0;
            int count = 1;
            String dynamicValue = null;
            while (elements.get(1).get(j) != null) {
                String uniqueValue = elements.get(1).get(j);
                dynamicValue = "dynamicvalue" + count;
                dynamicRequestBody = dynamicRequestBody.replace(dynamicValue, uniqueValue);
                j++;
                count++;
            }
        } catch (Exception e) {
            logger.error("All the values are dynamically replaced");
        }
        return dynamicRequestBody;
    }

    public Response initiatePostRestAPICall(String url) {
        try {
            if (reqHeaders == null) {
                response = given().relaxedHTTPSValidation().post(url);
            } else {
                response = given().headers(reqHeaders).post(url);
            }
            logger.debug("JSON Response::" + response.asString());

        } catch (Exception e) {
            response = null;
            logger.error("initiateRestAPICall exception msg::" + e.getMessage());
        }
        return response;
    }

    public Response initiatePutRestAPICall(String url, String jsonRequestFilePath) {
        try {
            String apiBody = convertJsonFileToString(
                    System.getProperty("user.dir") + "/src/test/resources/apiJsonRequest/" + jsonRequestFilePath
                            + ".json");
            if (reqHeaders == null) {
                response = given().relaxedHTTPSValidation().body(apiBody).put(url);
            } else {
                if (apiBody.isEmpty()) {
                    response = given().relaxedHTTPSValidation().headers(reqHeaders).put(url);
                } else {
                    response = given().relaxedHTTPSValidation().headers(reqHeaders).body(apiBody).put(url);
                }
            }
            logger.debug("JSON Response::" + response.asString());
        } catch (Exception e) {
            response = null;
            logger.error("initiateRestAPICall exception msg::" + e.getMessage());
            e.printStackTrace();
        }
        return response;
    }

    public Response initiatePutRestAPICall(String url) {
        try {
            if (reqHeaders == null) {
                response = given().relaxedHTTPSValidation().put(url);
            } else {
                response = given().relaxedHTTPSValidation().headers(reqHeaders).put(url);
            }
        } catch (Exception e) {
            response = null;
            logger.error("initiateRestAPICall exception msg::" + e.getMessage());
            e.printStackTrace();
        }
        return response;
    }

    public Response initiateRestDeleteAPICall(String url) {
        try {
            response = given().relaxedHTTPSValidation().headers(reqHeaders).when().delete(url);
        } catch (Exception e) {
            response = null;
            logger.error("initiateRestAPICall exception msg::" + e.getMessage());
            e.printStackTrace();
        }
        return response;
    }

    public static String convertJsonFileToString(String filePath) {
        String content = "";
        try {
            content = new String(Files.readAllBytes(Paths.get(filePath)));
        } catch (IOException e) {
            logger.error("convertJsonFileToString IOException msg::" + e.getMessage());
            e.printStackTrace();
        }
        return content;
    }

    /*
     * This recursive method will return a list of values that have matched with the key
     * */

    public static ArrayList<String> attributeMatchingRecursive(JsonNode root, String field, ArrayList<String> aryLst) {
        JsonNode fieldValue = null;
        if (root.isObject()) {
            Iterator<String> fieldNames = root.fieldNames();
            while (fieldNames.hasNext()) {
                String fieldName = fieldNames.next();
                fieldValue = root.get(fieldName);
                if (fieldName.equalsIgnoreCase(field)) {
                    aryLst.add(fieldValue.asText());
                }
                attributeMatchingRecursive(fieldValue, field, aryLst);
            }
        } else if (root.isArray()) {
            ArrayNode arrayNode = (ArrayNode) root;
            for (int i = 0; i < arrayNode.size(); i++) {
                JsonNode arrayElement = arrayNode.get(i);
                if (arrayElement.asText() == field) {
                    aryLst.add(fieldValue.textValue());
                }
                attributeMatchingRecursive(arrayElement, field, aryLst);
            }
        }
        return aryLst;
    }

    /*
     * This recursive method will return a list of values that have matched with the key and then we are storing
     * those list in a map that has been defined in the APIHandlingStepDef
     * */

    public static ArrayList<String> attributeMatchingRecursive(JsonNode root, String field) {
        ArrayList<String> aryLst = new ArrayList<>();
        JsonNode fieldValue = null;
        if (root.isObject()) {
            Iterator<String> fieldNames = root.fieldNames();
            while (fieldNames.hasNext()) {
                String fieldName = fieldNames.next();
                fieldValue = root.get(fieldName);
                if (fieldName.equalsIgnoreCase(field)) {
                    aryLst.add(fieldValue.asText());
                }
                aryLst.addAll(attributeMatchingRecursive(fieldValue, field));
            }
        } else if (root.isArray()) {
            ArrayNode arrayNode = (ArrayNode) root;
            for (int i = 0; i < arrayNode.size(); i++) {
                JsonNode arrayElement = arrayNode.get(i);
                if (arrayElement.asText() == field) {
                    aryLst.add(fieldValue.textValue());
                }
                aryLst.addAll(attributeMatchingRecursive(arrayElement, field));
            }
        }
        return aryLst;
    }

    /*
     * This recursive method will return a single value that have matched with the key and then it will break once
     * it found the matched key
     * */

    public static String attributeMatchingRecursiveSingleValue(JsonNode root, String field) {
        String value = "";
        JsonNode fieldValue = null;
        if (root.isObject()) {
            Iterator<String> fieldNames = root.fieldNames();
            while (fieldNames.hasNext()) {
                String fieldName = fieldNames.next();
                fieldValue = root.get(fieldName);
                if (fieldName.equalsIgnoreCase(field)) {
                    value = fieldValue.textValue();
                }
                if (StringUtils.isNotEmpty(value)) {
                    return value;
                }
                value = attributeMatchingRecursiveSingleValue(fieldValue, field);
            }
        } else if (root.isArray()) {
            ArrayNode arrayNode = (ArrayNode) root;
            for (int i = 0; i < arrayNode.size(); i++) {
                JsonNode arrayElement = arrayNode.get(i);
                if (arrayElement.asText() == field) {
                    value = fieldValue.textValue();
                }
                if (StringUtils.isNotEmpty(value)) {
                    return value;
                }
                value = attributeMatchingRecursiveSingleValue(arrayElement, field);
            }
        }
        return value;
    }

    public static String getValueFromJSon(final String response, final String node) throws IOException {
        Object nodeValue = null;
        try {
            JSONObject obj = new JSONObject(response);
            JSONObject jo = obj;
            try {
                // JSONObject dataObj = (JSONObject) jo.get("data");
                // nodeValue = dataObj.get(node);
                nodeValue = jo.get(node);
            } catch (Exception e) {
                nodeValue = jo.get(node);
            }
        } catch (NullPointerException e) {
            throw new NullPointerException("The argument cannot be null");
        }
        return nodeValue.toString();

    }

    public static String endPointFilteredValue(String urlPath, int num) {
        String s2 = null;
        String changedValue;
        String getKey = null;
        while (urlPath.contains("{")) {
            changedValue = attributes.get(num).get(1);
            if (changedValue.equalsIgnoreCase("newrecord")) {
                changedValue = dynamicValue;
            }
            //           else if(changedValue.contains("_"))
            //           {
            //              getKey=StringUtils.substringAfter(changedValue, "_");
            //              changedValue= getSDNewRecordsMap().get(getKey);
            //           }
            else if (changedValue.contains("$")) {
                changedValue = changedValue.replace("$", " ").trim();
                changedValue = webPropHelper.getTestDataProperty(changedValue).trim();
            } else {
                changedValue = attributes.get(num).get(1);
            }
            s2 = getStringFromString(urlPath);
            urlPath = urlPath.replaceFirst("[{]", "");
            urlPath = urlPath.replaceFirst("[}]", "");
            urlPath = urlPath.replaceFirst("(?:" + s2 + ")+", changedValue);
            num++;
        }
        return urlPath;
    }

    public static String getStringFromString(String s1) {
        String num;
        int start = 0;
        int end = 0;
        for (int i = 0; i < s1.length(); i++) {
            if (s1.charAt(i) == '{') {
                start = i;
            } else if (s1.charAt(i) == '}') {
                end = i;
                break;
            }
        }
        num = s1.substring(start + 1, end);
        return num;
    }

    public Response initiateDynamicPostApiWithPOJO(String url, Object body) {
        try {
            if (reqHeaders == null) {
                response = given().relaxedHTTPSValidation().body(body).post(url);
            } else {
                response = given().relaxedHTTPSValidation().headers(reqHeaders).body(body).post(url);
                logger.info(body);
            }

            logger.debug("JSON Response::" + response.asString());

        } catch (Exception e) {
            response = null;
            logger.error("initiateRestAPICall exception msg::" + e.getMessage());
        }
        return response;
    }

    public Response initiateDynamicPostApiWithPOJOOnEnvironment(String url, Object body, Environment env) {

        try {
            if (env.toString().equals("PRODUCTION")) {
                if (reqHeaders == null) {
                    responseProd = given().relaxedHTTPSValidation().body(body).post(url);
                } else {
                    responseProd = given().relaxedHTTPSValidation().headers(reqHeaders).body(body).post(url);
                    logger.info(body);
                }
                logger.debug("JSON Response::" + responseProd.asString());
                return responseProd;
            } else if (env.toString().equals("STAGING_QA")) {
                if (reqHeaders == null) {
                    responseStaging = given().relaxedHTTPSValidation().body(body).post(url);
                } else {
                    responseStaging = given().relaxedHTTPSValidation().headers(reqHeaders).body(body).post(url);
                    logger.info(body);
                }
                logger.debug("JSON Response::" + responseStaging.asString());
                return responseStaging;
            }
        } catch (Exception e) {
            responseProd = null;
            responseStaging = null;
            logger.error("initiateRestAPICall exception msg::" + e.getMessage());
        }
        return null;
    }

    public String getRequestBodyAsJson(Object request) throws JsonProcessingException {
        ObjectMapper objectMapper = new ObjectMapper();
        return objectMapper.writeValueAsString(request);
    }

    public String uniqueValueforPost(String jsonbody) {
        if (jsonbody.contains("uniquevalue")) {
            jsonbody = jsonbody.replaceAll("uniquevalue", getUniqueValue());
        } else if (jsonbody.contains("dynamicvalue")) {
            jsonbody = jsonbody.replaceAll("dynamicvalue", dynamicValue);
        }
        return jsonbody;
    }

    public String getUniqueValue() {
        getCurrnetMethodName = Thread.currentThread().getStackTrace()[1].getMethodName();
        String uniqueValue = null;
        try {
            formatter = new SimpleDateFormat("yyyyHHmmss");
            date = new Date();
            uniqueValue = formatter.format(date);

        } catch (Exception e) {
            logger.error(getFailedstep("FAILED-" + "<->" + getCurrnetMethodName + "<->" + "<->" + e));
        }
        return uniqueValue;

    }

    public static void assertingProdVersusQAResponse(Response responseQA, Response responseProd) throws IOException {
        JSONObject jsonQA = new JSONObject(responseQA.asString());
        JSONObject jsonProd = new JSONObject(responseProd.asString());
        JSONAssert.assertEquals(jsonProd, jsonQA, true);
        logger.info(" Response of QA/Staging versus Production is Equal with  no issues ");
    }

}
