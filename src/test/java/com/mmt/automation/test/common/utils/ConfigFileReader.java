package com.mmt.automation.test.common.utils;

public class ConfigFileReader {

	public String getReportConfigPath(){
		String reportConfigPath = System.getProperty("user.dir") + "/src/test/resources/config/extent-config.xml";
		return reportConfigPath;		
	}
}
