package com.mmt.automation.test.common.interfaces;

import io.restassured.response.Response;
import org.json.simple.parser.ParseException;

import java.io.IOException;
import java.util.List;

public interface APIParsersBaseClass {

    public void compareQAversusProdResponse(Response responseQA, Response responseProd)
            throws IOException, ParseException;
    public Object populateDataInJSONBody(List<List<String>> attributes);

}
