package com.mmt.automation.test.common.utils;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import javax.mail.*;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeMessage;
import java.io.IOException;
import java.util.Properties;
import javax.mail.internet.MimeBodyPart;
import javax.mail.internet.MimeMultipart;

public class GenerateMail {

    private static final Logger logger = LogManager.getLogger(GenerateMail.class);
    private static final PropertiesHelper webPropHelper = PropertiesHelper.getInstance();

    public static void main(String[] args) {
        Properties properties = System.getProperties();
        properties.setProperty("mail.smtp.host", "172.16.37.202");

        Session session = Session.getDefaultInstance(properties);

        try {
            Message message = new MimeMessage(session);
            message.setFrom(new InternetAddress("vikash.nowal@go-mmt.com"));
            message.addRecipient(Message.RecipientType.TO, new InternetAddress("karan.sharma@go-mmt.com"));
            message.addRecipient(Message.RecipientType.TO, new InternetAddress("vikash.nowal@go-mmt.com"));
            message.setSubject("Holidays Automation Tests Report");

            Multipart emailContent = new MimeMultipart();
            //Text body part
            MimeBodyPart textBodyPart = new MimeBodyPart();
            textBodyPart.setText("Today's Holidays Automation Tests Report");
            //Attachment body part.
            MimeBodyPart pdfAttachment = new MimeBodyPart();
            pdfAttachment.attachFile(System.getProperty("user.dir") + webPropHelper.getConfigPropProperty("reportPath") + "report.html");
            //Attach body parts
            emailContent.addBodyPart(textBodyPart);
            emailContent.addBodyPart(pdfAttachment);
            //Attach multipart to message
            message.setContent(emailContent);
            Transport.send(message);

            logger.debug("################ Automation Report Sent ###########################");
        }
        catch (MessagingException | IOException e) {
            e.printStackTrace();
        }
    }
}
