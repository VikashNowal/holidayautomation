package com.mmt.automation.test.common.helperservices;

import com.mmt.automation.api.test.parsers.details.ActivityListingParser;
import com.mmt.automation.api.test.parsers.details.CancellationPolicyParser;
import com.mmt.automation.api.test.parsers.details.DynamicDetailParser;
import com.mmt.automation.api.test.parsers.listing.DepartureCitiesParser;
import com.mmt.automation.api.test.parsers.listing.ListingMenuParser;
import com.mmt.automation.api.test.parsers.listing.ListingMetaParser;
import com.mmt.automation.api.test.parsers.listing.ListingPackagesParser;
import com.mmt.automation.api.test.pojo.details.CancellationPolicyRequest;
import com.mmt.automation.test.common.interfaces.APIParsersBaseClass;

public class APIParserObjectMaker {

    public static APIParsersBaseClass getClassObjectOfAPI(String APIname) {
        switch (APIname) {
        case "DynamicDetails":
            return DynamicDetailParser.getInstance();
        case "CancellationDetail":
            return CancellationPolicyParser.getInstance();
        case "DepartureCities":
            return DepartureCitiesParser.getInstance();
        case "ListingPackage":
            return ListingPackagesParser.getInstance();
        case "ListingMenu":
            return ListingMenuParser.getInstance();
        case "ListingMeta":
            return ListingMetaParser.getInstance();
        case "ActivityChangeListing":
            return ActivityListingParser.getInstance();
        default:
            return null;
        }
    }
    
    public static APIParsersBaseClass getClassObjectOfDto(String APIname) {
        switch (APIname) {
        case "CancellationPolicyRequest":
        	CancellationPolicyRequest cancellationPolicy = new CancellationPolicyRequest();
        	return (APIParsersBaseClass) cancellationPolicy;
        default:
            return null;
        }
    }
    
    
    
    
    
    
}
