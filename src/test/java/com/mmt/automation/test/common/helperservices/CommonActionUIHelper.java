package com.mmt.automation.test.common.helperservices;

import com.mmt.automation.test.common.Enums.LocatorTypeEnum;
import com.mmt.automation.test.common.utils.PropertiesHelper;
import com.mmt.automation.test.common.utils.WebDriverHelper;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.junit.Assert;
import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.NoSuchElementException;
import org.openqa.selenium.TimeoutException;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

public class CommonActionUIHelper {

    private static final Logger logger = LogManager.getLogger(CommonActionUIHelper.class);
    private final PropertiesHelper webPropHelper = PropertiesHelper.getInstance();
    private final WebDriverHelper webDriverHelperObj = WebDriverHelper.getInstance();
    public String getCurrnetMethodName;
    private WebDriverWait wait;
    public static final String FAILEDSTEP = "This test-step has been failed";
    private static CommonActionUIHelper commonActionHelperObj = null;

    public static CommonActionUIHelper getInstance() {
        if (commonActionHelperObj == null)
            commonActionHelperObj = new CommonActionUIHelper();
        return commonActionHelperObj;
    }

    public static String getAPIFailedstep(String e) {
        throw new AssertionError("exception in API call Failed " + "- " + e);
    }

    public static void getAPIPassedstep(String e) {
        logger.info("Step is passed in API-> " + "- " + e);
    }

    public static String getFailedstep(WebElement ele) {
        throw new AssertionError(FAILEDSTEP + " - " + ele);
    }

    public static String getFailedstep(String e) {
        throw new AssertionError("Element Not found -> " + "- " + e);

    }

    public static String getFailedstep(Exception e) {
        throw new AssertionError(FAILEDSTEP + " - " + e);
    }

    public static String getFailedstep(Exception e, WebElement el) {
        throw new AssertionError(FAILEDSTEP + " - " + e + " - " + el);
    }

    public WebElement getBDDElement(String currentElement, String pageName) {
        WebElement webElement = null;
        String typeofLocator = null;
        Class<?> className = null;
        try {
            className = Class.forName("com.mmt.automation.test.pageobject." + pageName);
        } catch (ClassNotFoundException e) {
            logger.debug("The class " + pageName + " isn't available on classpath");
            e.printStackTrace();
            getFailedstep(e);
        }
        try {
            FindBy annotationValue = className.getField(currentElement).getAnnotation(FindBy.class);

            typeofLocator = (currentElement.split("\\_")[0]);

            By locator = null;
            LocatorTypeEnum ltEnum = LocatorTypeEnum.valueOf(typeofLocator.toLowerCase());
            switch (ltEnum) {
            case id:
                locator = By.id(annotationValue.id());
                break;
            case name:
                locator = By.name(annotationValue.name());
                break;
            case css:
                locator = By.cssSelector(annotationValue.css());
                break;
            case linkText:
                locator = By.linkText(annotationValue.linkText());
                break;
            case xpath:
                locator = By.xpath(annotationValue.xpath());
                break;
            default:
                locator = By.xpath(annotationValue.xpath());
                break;
            }

            Assert.assertTrue(waitForElement(currentElement, locator));
            webElement = webDriverHelperObj.driver.findElement(locator);
        } catch (NoSuchElementException e) {
            getFailedstep(e, webElement);
        } catch (Exception e) {
            getFailedstep(e, webElement);
        }

        return webElement;
    }

    public boolean scrollToTopBottomPage(String position) {
        getCurrnetMethodName = Thread.currentThread().getStackTrace()[1].getMethodName();

        boolean flag = false;
        try {
            if (position.equalsIgnoreCase("End")) {
                ((JavascriptExecutor) webDriverHelperObj.driver)
                        .executeScript("window.scrollTo(0, document.body.scrollHeight)");
                flag = true;
                logger.info("PASSED -: " + position);

            } else if (position.equalsIgnoreCase("Top")) {
                ((JavascriptExecutor) webDriverHelperObj.driver)
                        .executeScript("window.scrollTo(document.body.scrollHeight, 0)");
                flag = true;
                logger.info("PASSED -: " + position);
            }

        } catch (Exception e) {
            logger.error(getFailedstep("FAILED-" + "<->" + getCurrnetMethodName + "<->" + position + "<->" + e));
        }
        return flag;
    }

    public boolean isDisplayed(WebElement element) {
        boolean isDisplayedFlag = false;
        getCurrnetMethodName = Thread.currentThread().getStackTrace()[1].getMethodName();

        try {
            // scrollPageToWebElement(element);
            isDisplayedFlag = element.isDisplayed();
            if (isDisplayedFlag) {
                logger.info("PASSED -: " + element);
            } else {
                logger.error(getFailedstep("FAILED-" + getCurrnetMethodName + "<->" + element));

            }
        } catch (Exception e) {
            logger.error(getFailedstep("FAILED-" + getCurrnetMethodName + "<->" + element + "<->" + e));
        }
        return isDisplayedFlag;
    }

    public boolean isEnabled(WebElement element) {
        boolean isEnabledFlag = false;
        getCurrnetMethodName = Thread.currentThread().getStackTrace()[1].getMethodName();

        try {
            // scrollPageToWebElement(element);
            isEnabledFlag = element.isEnabled();
            if (isEnabledFlag) {
                logger.info("PASSED -: " + element);
            } else {
                logger.error(getFailedstep("FAILED-" + getCurrnetMethodName + "<->" + element));

            }
        } catch (Exception e) {
            logger.error(getFailedstep("FAILED-" + getCurrnetMethodName + "<->" + element + "<->" + e));
        }
        return isEnabledFlag;
    }

    public boolean isClickable(WebElement element) {
        try {
            wait = new WebDriverWait(webDriverHelperObj.driver, 2000);
            wait.until(ExpectedConditions.elementToBeClickable(element));
            logger.debug("Element is clickable true");
            return true;
        } catch (Exception e) {
            logger.debug("Element is clickable false");
            return false;
        }
    }

    public boolean clickOnButton(WebElement element) {
        getCurrnetMethodName = Thread.currentThread().getStackTrace()[1].getMethodName();
        boolean flag = false;
        try {
            flag = true;
            if (isClickable(element)) {
                element.click();
                flag = true;
            } else {
                logger.error(getFailedstep("FAILED-" + "<->" + getCurrnetMethodName + "<->" + element));

            }
        } catch (NoSuchElementException e) {
            logger.error(getFailedstep("FAILED-<->" + getCurrnetMethodName + "<->" + element + "<->" + e));
        } catch (Exception e) {
            logger.error(getFailedstep("FAILED-<->" + getCurrnetMethodName + "<->" + element + "<->" + e));
        }
        return flag;
    }

    public boolean setInputText(WebElement element, String text) {
        boolean flag = false;
        getCurrnetMethodName = Thread.currentThread().getStackTrace()[1].getMethodName();

        logger.info("Input the text box value : " + text);
        try {
            if (element.isEnabled()) {
                element.clear();
                element.sendKeys(text);
                flag = true;

            } else {
                logger.error(getFailedstep("FAILED-+<->" + getCurrnetMethodName + "<->" + element));

            }
        } catch (Exception e) {
            logger.error(getFailedstep("FAILED-+<->" + getCurrnetMethodName + "<->" + element + "<->" + e));
        }
        return flag;
    }

    public boolean setInputTextWithoutClear(WebElement element, String text) {
        boolean flag = false;
        getCurrnetMethodName = Thread.currentThread().getStackTrace()[1].getMethodName();

        logger.info("Input the text box value : " + text);
        try {
            if (element.isEnabled()) {
                element.sendKeys(text);
                flag = true;

            } else {
                logger.error(getFailedstep("FAILED-+<->" + getCurrnetMethodName + "<->" + element));

            }
        } catch (Exception e) {
            logger.error(getFailedstep("FAILED-+<->" + getCurrnetMethodName + "<->" + element + "<->" + e));
        }
        return flag;
    }

    public boolean moveHover(WebElement element) {
        getCurrnetMethodName = Thread.currentThread().getStackTrace()[1].getMethodName();
        boolean flag = false;
        try {
            logger.info("Hover on an element");
            Actions actions = new Actions(webDriverHelperObj.driver);
            actions.moveToElement(element).perform();
            flag = true;
        } catch (Exception e) {
            logger.error(getFailedstep(element + "<->" + e));
        }
        return flag;
    }

    protected void clearInputBox(WebElement element) {
        logger.info("Clear the Input box value");
        try {
            if (element.isEnabled()) {
                element.clear();
            } else {
            }
        } catch (Exception e) {
            logger.error("Exception in  clearInputBox msg::" + e.getMessage());
        }
    }

    public boolean waitForElement(String currentElementName, By element) {
        getCurrnetMethodName = Thread.currentThread().getStackTrace()[1].getMethodName();
        boolean flag = false;
        if (element != null) {
            try {
                long waitTime = Long.parseLong(webPropHelper.getConfigPropProperty("ElementWait"));
                wait = new WebDriverWait(webDriverHelperObj.driver, waitTime); // Wait for 20 seconds
                wait.until(ExpectedConditions.presenceOfElementLocated(element));
                logger.info("PASSED -: " + element);

                flag = true;
            } catch (NoSuchElementException e) {
                flag = false;
                logger.error(getFailedstep(
                        "FAILED-" + currentElementName + "<->" + getCurrnetMethodName + "<->" + element + "<->" + e));

            } catch (TimeoutException e) {
                flag = false;
                logger.error(getFailedstep(
                        "FAILED-" + currentElementName + "<->" + getCurrnetMethodName + "<->" + element + "<->" + e));
            }
        }
        return flag;
    }

    public boolean scrollPageToWebElement(WebElement element) {
        boolean flag = false;
        getCurrnetMethodName = Thread.currentThread().getStackTrace()[1].getMethodName();
        try {
            if (element != null) {
                ((JavascriptExecutor) webDriverHelperObj.driver)
                        .executeScript("arguments[0].scrollIntoView(true);", element);
                logger.info("PASSED -: " + element);

                flag = true;
            } else {
                flag = false;
                logger.info("FAILED -: " + element);
            }
        } catch (Exception e) {
            logger.error(getFailedstep("FAILED- " + getCurrnetMethodName + "<->" + element + "<->" + e));
        }
        return flag;
    }

}
