package com.mmt.automation.test.common.utils;

import java.io.FileInputStream;
import java.io.IOException;
import java.util.Properties;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

public class PropertiesHelper {

	private static final Logger logger = LogManager.getLogger(PropertiesHelper.class);
	private final Properties configProp = new Properties();
	private final Properties testDataProperty = new Properties();
	private final Properties apiEndPoints = new Properties();

	// Bill Pugh Solution for singleton pattern
	private static class LazyHolder {
		private static final PropertiesHelper INSTANCE = new PropertiesHelper();
	}

	public static PropertiesHelper getInstance() {
		return LazyHolder.INSTANCE;
	}

	private PropertiesHelper() {
		logger.debug("Read all properties from file");
		try {

			FileInputStream configPropFis = getFileInputStrem(
					System.getProperty("user.dir") + "/src/test/resources/config/Config.properties");
			if (configPropFis != null) {
				configProp.load(configPropFis);
			}
			logger.debug("properties file load Done.");

			String testdataPropFileName = System.getProperty("testdatafilename");
			if (testdataPropFileName == null) {
				testdataPropFileName = getConfigPropProperty("testdatafilename");
			}
			logger.debug("testdatafilename::" + testdataPropFileName);

			FileInputStream testDataPropFis = getFileInputStrem(
					System.getProperty("user.dir") + "/src/test/resources/TestData/" + testdataPropFileName);
			if (testDataPropFis != null) {
				testDataProperty.load(testDataPropFis);
			}
			
			FileInputStream apiEndPointFis = getFileInputStrem(
					System.getProperty("user.dir") + "/src/test/resources/object_repo/api/EndPoints.properties");
			if (apiEndPointFis != null) {
				apiEndPoints.load(apiEndPointFis);
			}

		} catch (IOException e) {
			logger.error("PropertiesHelper IOException:: " + e.getMessage());
			e.printStackTrace();
		}

	}

	public String getConfigPropProperty(String key) {
		return configProp.getProperty(key);
	}

	public String getTestDataProperty(String key) {
		return testDataProperty.getProperty(key);
	}
	
	public String getEndpointProProperty(String key) {
		return apiEndPoints.getProperty(key);
	}

	private FileInputStream getFileInputStrem(String filePath) {
		FileInputStream fileInputStrem = null;
		try {
			fileInputStrem = new FileInputStream(filePath);
		} catch (Exception e) {
			logger.error("getFileInputStrem() exception msg::" + e.getMessage());
			logger.error("FILE NOT FOUND::" + filePath);
		}
		return fileInputStrem;
	}

}
