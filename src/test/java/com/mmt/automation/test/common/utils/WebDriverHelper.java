package com.mmt.automation.test.common.utils;

import java.io.File;
import java.io.FileNotFoundException;
import java.net.MalformedURLException;
import java.util.concurrent.TimeUnit;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebDriverException;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.ie.InternetExplorerDriver;
import org.openqa.selenium.remote.DesiredCapabilities;


public class WebDriverHelper {
	
	private static final Logger logger = LogManager.getLogger(WebDriverHelper.class);
	public WebDriver driver;
	public static String browserType;
	public static String applicationUrl;
	private final PropertiesHelper webPropHelper = PropertiesHelper.getInstance();
	private static WebDriverHelper webDriverHelperObj = null;

	public static WebDriverHelper getInstance() {
		if (webDriverHelperObj == null)
			webDriverHelperObj = new WebDriverHelper();
		return webDriverHelperObj;
	}
	
	@SuppressWarnings("deprecation")
	public void open_web_driver(String applicationName) throws MalformedURLException, InterruptedException, FileNotFoundException {
	
		browserType = System.getProperty("browser");
		
		if(browserType == null) {
		browserType = webPropHelper.getConfigPropProperty("BrowserType");
		}
		applicationUrl = webPropHelper.getConfigPropProperty(applicationName);
		
		try {
			
			if (browserType.equalsIgnoreCase("firefox")) {
				File file = new File(System.getProperty("user.dir")+"/src/test/resources/drivers/windows/geckodriver.exe");
				System.setProperty("webdriver.gecko.driver", file.getAbsolutePath());
				DesiredCapabilities capabilities = new DesiredCapabilities();
				capabilities.setCapability("marionette", true);
				driver = new FirefoxDriver(capabilities);
			} else if (browserType.equalsIgnoreCase("chrome")) {
//				ChromeOptions options = new ChromeOptions();
//				options.setExperimentalOption("excludeSwitches", Collections.singletonList("enable-automation"));
//				options.addArguments("disable-infobars");
//				options.addArguments("--incognito");
//				options.addArguments("start-maximized");
//				options.addArguments("--disable-extensions");
				File file = new File(System.getProperty("user.dir")+"/src/test/resources/drivers/windows/chromedriver.exe");
				System.setProperty("webdriver.chrome.driver", file.getAbsolutePath());
				driver = new ChromeDriver();
				driver.manage().window().maximize();  

			} else if (browserType.equalsIgnoreCase("IE")) {
				File file = new File(System.getProperty("user.dir")+"/src/test/resources/drivers/windows/IEDriverServer.exe");
				System.setProperty("webdriver.ie.driver", file.getAbsolutePath());
				DesiredCapabilities capabilities = DesiredCapabilities.internetExplorer();
				capabilities.setCapability(InternetExplorerDriver.INTRODUCE_FLAKINESS_BY_IGNORING_SECURITY_DOMAINS,
						true);
				capabilities.setCapability("requireWindowFocus", true);
				driver = new InternetExplorerDriver(capabilities);
			}
			driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
			driver.manage().window().maximize();
			driver.get(applicationUrl);
			driver.navigate().refresh();
		} catch (WebDriverException e) {
			System.out.println(e.getMessage());
		} catch (Throwable t) {
			// Selenium puts blank lines in its exceptions. Remove them and
			// rethrow.
			throw new RuntimeException(t.getMessage().replaceAll("(?m)^[ \t]*\r?\n", ""));
		}
	}	
	
	public void quitDriver() 
	{
		logger.debug("inside teardown()....");
		if (driver != null) 
		{
			driver.quit();
			logger.debug("driver quit success.");
		}
	}
	
		
}
