package com.mmt.automation.test.runner;

import java.io.*;
import org.junit.AfterClass;
import org.junit.runner.RunWith;
import com.cucumber.listener.Reporter;
import cucumber.api.CucumberOptions;
import cucumber.api.junit.Cucumber;

@RunWith(Cucumber.class)
@CucumberOptions
(      features="src/test/resources/features/API",
		glue={"com.mmt.automation.api.test.stepdefinitions", "com.mmt.automation.test.common"},
		plugin = { "com.cucumber.listener.ExtentCucumberFormatter:target/cucumber-reports/report.html"},
		monochrome=true,
	 //,dryRun = true ,
		tags="@ListingPackage_TC1"
		) 


public class APITestRunner {

}
