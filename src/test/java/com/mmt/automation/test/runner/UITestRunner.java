package com.mmt.automation.test.runner;

import java.io.*;
import org.junit.AfterClass;
import org.junit.runner.RunWith;
import com.cucumber.listener.Reporter;
import cucumber.api.CucumberOptions;
import cucumber.api.junit.Cucumber;

@RunWith(Cucumber.class)
@CucumberOptions
(      features="src/test/resources/features/UI",
		glue={"com.mmt.automation.ui.test.stepdefinitions", "com.mmt.automation.test.common"},
		plugin = { "com.cucumber.listener.ExtentCucumberFormatter:target/cucumber-reports/report.html"},
		monochrome=true,
	 //,dryRun = true ,
		tags="@DemoUI "
		) 
public class UITestRunner {

	@AfterClass
	public static void writeExtentReport() {
		Reporter.loadXMLConfig(new File(System.getProperty("user.dir") + "/src/test/resources/config/extent-config.xml"));
	}
}
